// *3@lî@f;~8+=-2)as,32!$#@&!@#*(()()|_\\/*<g5$1ghj45#000-()()|~6*!@}$#'?ghj45_\\/*fC|/L+=-2)as,32&$(s4}$#'?+=-2)2w\`%_\\/*TdEa8_\\/*ybdfe32dsw#6*dUybdfeybdfe#6*dU9)7tg*7@6&ybdfe!'a**
// "1-nirA/ tg l21goRe03e on1/sHdc51 era" -> 6
package programabiblioteca;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Alencar
 */
public class newBanco {

    private static DB db;

    public newBanco() {
        db = new DB("admin", "admin", "org.h2.Driver", "jdbc:h2:tcp://" + System.getProperty("user.dir") + "\\data");
    }

    /**
     * Este métedo irá retornar `true´ no caso do livro estar retirado, para que
     * então só assim possa ser efetuada a devolução, pois se o livro não foi
     * retirado, não à como devolve-lo.
     *
     * @param ID
     * @return boolean
     */
    public static boolean livro_retirado_N(String ID) {
        boolean TF = false;                           //
        try (ResultSet rs = pegaDadosNoBD("SELECT * FROM HISTORICO_RECENTE WHERE HR_LIVRO = " + ID + "")) {
            while (rs.next()) {
                TF = true;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nbanco.livroRetiradoN");
        }

        return TF;
    }

    /**
     * Este método irá ver se o código do aluno existe, no caso de não exisitr
     * ele retorna `true´.
     *
     * @param ID
     * @return boolean
     */
    public static boolean verifica_cod_aluno(String ID) {
        boolean TF = true;                        //
        try (ResultSet rs = pegaDadosNoBD("select * from ALUNOS where AL_ID = " + ID)) {
            while (rs.next()) {
                TF = false;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nbanco.verificaCodAluno");
        }
        return TF;
    }

    /**
     * Este método procura o código do livro, no caso de não existir um livro
     * com aquele código, retornara `true´.
     *
     * @param ID
     * @return boolean
     */
    public static boolean procura_cod_livro(String ID) {
        boolean TF = true;
        try (ResultSet rs = pegaDadosNoBD("select * from LIVROS where LI_CODIGO = " + ID)) {
            while (rs.next()) {
                TF = false;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nbanco.procuraCodLivro");
        }
        return TF;
    }

    /**
     * Este método serve para pegar qual o sexo do aluno (no Banco de Dados, 'M'
     * ou 'F') para assim mostrar, ou editar... no final, ele retorna 'M' ou 'F'
     *
     * @param ID
     * @return String
     */
    public static String pega_tipo(String ID) {
        String sexo = "";
        try (ResultSet rs = pegaDadosNoBD("select * from ALUNOS where AL_ID = " + ID)) {
            rs.next();
            sexo = rs.getString("al_sexo");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nbanco.pegaTipo");
        }
        return sexo;
    }

    /**
     * Este método pega o nome da turma, ou do turno, tanto faz, pois será
     * especificado na hora da chamada do mesmo... e no final irá retornar a
     * turma ou o turno.
     *
     * @param num
     * @param txt
     * @return String
     */
    public static String pega_nome_turma_turno(int num, String txt) {
        String op = txt, nome = "", column;
        try {
            column = txt.equals("turmas") ? "ta_" : "to_";
            String cmd = "select * from " + op + " where " + column + "id = " + num;
            try (ResultSet rs = pegaDadosNoBD(cmd)) {
                rs.first();
                nome = rs.getString(column + "NAME");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nbanco.pegaNome_TurmaTurno");
        }
        return nome;
    }

    /**
     * Este método irá pegar o id do turno, ou da turma, caso for 0 ou 1... ele
     * irá fazer a pesquisar no banco de dados, por um texto digitado pelo
     * usuário e fará a comparação com os turnos ou turmas que foi requisitado.
     * Retornando no final, o ID da turma ou do turno.
     *
     * @param num
     * @param Turma_Turno
     * @return int
     */
    public static int pega_id_turma_turno(int num, String Turma_Turno) {
        String op = "", column = "";
        int contador_ID = 0;
        if (num == 0) {
            op = "turmas";
            column = "ta_";
        } else if (num == 1) {
            op = "turnos";
            column = "to_";
        }
        try {
            try (ResultSet rs = pegaDadosNoBD("select * from " + op + " where " + column + "NAME LIKE '%" + Turma_Turno + "%'")) {
                while (rs.next()) {
                    contador_ID = Integer.parseInt(rs.getString(column + "ID"));
                }
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nbanco.pegaId_TurmaTurno");
        }
        return contador_ID;
    }

    /**
     * Recebe um comando que precisa ser executado no Banco de Dados e o
     * executa.
     *
     * @param comando
     */
    public static void insere(String comando) {
        db.insereComando(comando);
    }

    /**
     * Este método irá procurar no banco de dados o nome do livro, por meio do
     * seu ID, e retornar o seu nome.
     *
     * @param ID
     * @return String
     */
    public static String pega_nome_livro(String ID) {
        String nome = "";
        try (ResultSet rs = pegaDadosNoBD("SELECT * FROM LIVROS WHERE LI_CODIGO = " + ID + "")) {
            rs.next();
            nome = rs.getString("li_nome");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nbanco.pegaNomeLivro");
        }
        return nome;
    }

    /**
     * Este método serve para ver se o aluno tem algum livro retirado, no caso
     * do aluno tiver algum livro retirado retorna ´false`.
     *
     * @param ID
     * @return boolean
     */
    public static boolean aluno_retirou_N(String ID) {
        boolean TF = true;
        try (ResultSet rs = pegaDadosNoBD("SELECT * FROM historico_recente WHERE hr_aluno = " + ID + "")) {
            while (rs.next()) {
                TF = false;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nbanco.alunoRetirouN");
        }
        return TF;
    }

    /**
     * Este método serve para abrir o banco de dados antes que o resto do
     * programa, pois para a inicialização do programa, é necessário o uso do
     * banco de dados.
     */
    public static void executarComandos() {
        String comando = "cmd /c java -jar lib\\database.jar";
        try {
            Process exec = Runtime.getRuntime().exec(comando);
        } catch (Exception e) {
            System.exit(0);
        }
    }

    /**
     * Encerra um processo caso ele exista
     *
     * @param processo
     * @return boolean
     */
    public static boolean encerraProcessoBancoDados(String processo) {
        try {
            String line;
            Process p = Runtime.getRuntime().exec("tasklist.exe /fo csv /nh");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                if (!line.trim().equals("")) {
                    if (line.substring(1, line.indexOf("\"", 1)).equals(processo)) {
                        Runtime.getRuntime().exec("taskkill /F /IM " + line.substring(1, line.indexOf("\"", 1)));
                        return true;
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Se 'num' for igual a 0, retorna o número de dias que o livro foi
     * retirada. Se 'num' for igual a 1 retorna a data da retirada
     *
     * @param num
     * @param codigo
     * @return String
     */
    public static String pega_num_dias(int num, String codigo) {
        String nome = "";
        try (ResultSet rs = pegaDadosNoBD("SELECT * FROM historico_recente WHERE hr_livro = " + codigo + "")) {
            while (rs.next()) {
                if (num == 0) {
                    nome = rs.getString("HI_DIAS");
                } else if (num == 1) {
                    nome = rs.getString("HI_DATA");
                }
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nbanco.pegaNumDias");
        }
        return nome;
    }

    /**
     * Grava no banco de dados um log de retirada.
     *
     * @param id_aluno
     * @param id_livro
     */
    public static void geraLog(String id_aluno, String id_livro) {
        try {
            data date = new data();
            String log_data = date.obtemDataHoraPadraoReversa().replace("/", "-");
            String comando = "insert into HISTORICO_COMPLETO (hc_aluno, hc_livro, hc_data) values (" + id_aluno + ", " + id_livro + ", '" + log_data + "')";
            insere(comando);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERRO: " + e + " \nbanco.geraLog");
        }
    }

    /**
     * Retorna o numero de livros/alunos existentes no banco de dados
     *
     * @param tipo
     * @return int
     */
    public static int numero_AlunosLivros(String tipo) {
        int cont;
        try (ResultSet rs = pegaDadosNoBD("select * from " + tipo)) {
            rs.last();// vai pra ultima linha da tabela LIVROS ou ALUNOS
            cont = rs.getRow();// pega o numero da ultima linha para retornar
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERRO: " + e + " \nbanco.numeroAlunosLivros");
            return 0;
        }
        return cont;
    }

    /**
     * Retorna o nome do aluno a partir de seu ID
     *
     * @param ID
     * @return String
     */
    public static String pega_nome_aluno(String ID) { //Este método irá procurar no banco de dados o nome do aluno, por meio do seu ID,
        String nome;
        try (ResultSet rs = pegaDadosNoBD("SELECT * FROM ALUNOS WHERE AL_ID = " + ID + "")) {
            rs.first();
            nome = rs.getString("NOME_ALU");
        } catch (SQLException ex) {
            nome = "ALUNO NÃO ENCONTRADO";
        }
        return nome;
    }

    /**
     * Cria as tabales no banco de dados caso elas ainda não existam.
     */
    public static void criaTabelasBD() {
//        String cmd;
//        cmd = "create table IF NOT EXISTS alunos ( ID int auto_increment primary key, NOME_ALU varchar (55), TURMA_ALU varchar(3), TURNO_ALU varchar(3), SEXO_ALU char(1) );";
//        insere(cmd);
//        cmd = "create table IF NOT EXISTS turmas ( ID int auto_increment primary key, NAME varchar (30) );";
//        insere(cmd);
//        cmd = "create table IF NOT EXISTS turno  ( ID int auto_increment primary key, NAME varchar (30) );";
//        insere(cmd);
//        cmd = "create table IF NOT EXISTS livros (  ID int auto_increment , Codigo int primary key,  NOME_LI varchar(55), AUTOR_LI varchar(50));";
//        insere(cmd);
//        cmd = "create table IF NOT EXISTS reti_devo (   ID int auto_increment primary key, ID_aluno int, ID_livro int, data varchar(10), dias int );";
//        insere(cmd);
//        cmd = "create table IF NOT EXISTS logs ( ID int auto_increment primary key, ID_ALUNO int, ID_LIVRO int, DATA varchar(30) );";
//        insere(cmd);
//        cmd = "create table IF NOT EXISTS seguranca (chave varchar(500));";
//        insere(cmd);
    }

    /**
     * Executa um comando no banco de dados e retorna o seu resultado
     *
     * @param comando
     * @return ResultSet
     */
    public static ResultSet pegaDadosNoBD(String comando) {
        return db.getInfoOfDB(comando);
    }

    public static void limpaLogs() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        try {
            ResultSet logs = pegaDadosNoBD("select * from HISTORICO_RECENTE");
            while (logs.next()) {
                Date date1 = sdf.parse(logs.getString("HI_DATA").substring(0, 10));
                data.le_data();
                String b = data.ano + "/" + data.mes + "/" + data.dia;
                Date date2 = sdf.parse(b);
                long differenceMilliSeconds = date2.getTime() - date1.getTime();
                int num_dias = (int) (differenceMilliSeconds / 1000 / 60 / 60 / 24);

                if (num_dias > 40) {
                    insere("delete from HISTORICO_RECENTE where hr_id = " + logs.getString("ID"));
                }
            }
            logs.close();
        } catch (ParseException | SQLException e) {

        }
    }
}
