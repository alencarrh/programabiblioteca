
/**
 * Author:  Alencar Rodrigo Hentges <alencarhentges@gmail.com>
 * Created: 18/03/2016
 */


CREATE TABLE turmas(    --- [turmas]
    ta_id int auto_increment,       --id da turma                       --- [ID]
    ta_nome varchar(55) not null,   --nome da turma                     --- [NAME]
    ta_ordem int,                   --ordem -> 1,2,3,4,...,n            --- [novo]
    ta_bloqueado varchar(1),        --indica se o campo está bloqueado  --- [novo]
    PRIMARY KEY (ta_id)
);
CREATE TABLE turnos(    --- [turno]
    to_id int auto_increment,       --id do turno                       --- []
    to_nome varchar(55) not null,   --nome do turno                     --- []
    to_bloqueado varchar(1),        --indica se o campo está bloqueado  --- [novo]
    PRIMARY KEY(to_id)
);
CREATE TABLE SEGURANCA( --- [seguranca]
    se_text varchar(1000),  --text with a encrypted message             --- [chave]
    se_key varchar(10)      --key for break the chave                   --- []
);
CREATE TABLE  alunos(   --- [alunos]
    al_id int auto_increment,           --id do aluno                   --- [ID]
    al_nome varchar(55) not null,       --nome do aluno                 --- [NOME_ALU]
    al_turma int not null,              --id da turma                   --- [TURMA_ALU]
    al_turno int not null,              --id do turno                   --- [TURNO_ALU]
    al_sexo varchar(1),                 --sexo do aluno                 --- [SEXO_ALU]
    al_passou varchar(1) default null,  --1 se passou naquele ano; 0 se reprovou    --- [novo]
    al_bloqueado varchar(1),            --indica se o campo está bloqueado          --- [novo]
    PRIMARY KEY (al_id),
    FOREIGN KEY (al_turma) REFERENCES public.turmas(ta_id),
    FOREIGN KEY (al_turno) REFERENCES public.turnos(to_id)
);
CREATE TABLE livros(    --- [livros]
    li_id int auto_increment,          --id do livro                    --- [ID]
    li_codigo int not null,            --codigo do livro                --- [codigo]
    li_nome varchar(100) not null,     --nome do livro                  --- [NOME_LI]   
    li_autor varchar(55) not null,     --nome do autor                  --- [AUTOR_LI]
    li_bloqueado varchar(1),           --indica se o campo está bloqueado   --- [novo]
    PRIMARY KEY (li_id)
);
CREATE TABLE emprestimos(   --- [reti_devo] 
    em_id int auto_increment,       --id do imprestimo                  --- [ID]
    em_aluno int not null,          --id do aluno                       --- [ID_aluno]
    em_livro int not null,          --id do livro                       --- [ID_livro]
    em_data TIMESTAMP not null,     --data da retirada                  --- [data]
    em_dias int not null,           --número de dias                    --- [dias]
    em_bloqueado varchar(1),        --indica se o campo está bloqueado  --- [novo]
    PRIMARY KEY (em_id),
    FOREIGN KEY (em_aluno) REFERENCES public.alunos(al_id),
    FOREIGN KEY (em_livro) REFERENCES public.livros(li_codigo)
);
CREATE TABLE historico(     --- [logs]
    hi_id int auto_increment        --id do registro                    --- [ID]
    hi_aluno int not null,          --id do aluno                       --- [ID_ALUNO]
    hi_livro int not null,          --id do livro                       --- [ID_LIVRO]
    hi_data TIMESTAMP not null,     --data de cadastro do registro      --- [DATA]
    hi_acao char(1) not null,       --acao(retidada ou devolução)       --- [{existe, mas não tenho mapeado}]
    hi_bloqueado varchar(1),        --indica se o campo está bloqueado  --- [novo]
    PRIMARY KEY (hi_id),
    FOREIGN KEY (hi_aluno) REFERENCES public.alunos(al_id),
    FOREIGN KEY (hi_livro) REFERENCES public.livros(li_codigo)
);








--Buscas fazendo a junção de tabelas.

----> Busca informação completa dos alunos
SELECT al_nome, ta_nome, to_nome, al_sexo
   FROM alunos
INNER JOIN turmas
   ON alunos.al_turma = turmas.ta_id
INNER JOIN turnos
   ON alunos.al_turno = turnos.to_id
WHERE al_bloqueado = "NÃO BLOQUADO";

---->pega info livro com historico recente
SELECT em_data, li_codigo, li_nome, al_id
   FROM emprestimos
INNER JOIN livros
   ON emprestimos.hi_livro = livros.li_codigo
INNER JOIN alunos
   ON emprestimos.hi_aluno = alunos.al_id
WHERE hi_aluno = al_id;

--->PEGA INFORMAÇÕES 
SELECT EM_ALUNO, EM_LIVRO, AL_NOME, AL_ID 
   FROM EMPRESTIMO
INNER JOIN alunos 
   ON EM_ALUNO = AL_ID 
WHERE EM_LIVRO = 123


----> Inserir TIMESTAMP sem utilizar tempo atual
insert into historico  values (null, 10,10, CONVERT('2014-07-02 06:14:00', TIMESTAMP));





----> deleta todas as tabelas
drop table ALUNOS ;
drop table HISTORICO_COMPLETO ;
drop table HISTORICO_RECENTE ;
drop table LIVROS ;
drop table SEGURANCA ;
drop table TURMAS ;
drop table TURNOS ;