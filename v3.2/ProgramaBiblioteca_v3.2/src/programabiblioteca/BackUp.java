// *3@lî@f;~8+=-2)as,32!$#@&!@#*(()()|_\\/*<g5$1ghj45#000-()()|~6*!@}$#'?ghj45_\\/*fC|/L+=-2)as,32&$(s4}$#'?+=-2)2w\`%_\\/*TdEa8_\\/*ybdfe32dsw#6*dUybdfeybdfe#6*dU9)7tg*7@6&ybdfe!'a**
// "1-nirA/ tg l21goRe03e on1/sHdc51 era" -> 6
package programabiblioteca;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * @brief Classe BackUp
 * @author Alencar Rodrigo Hentges <alencarhentges@gmail.com>
 * @date 13/11/2015
 */
public class BackUp {

    public static void makeBackup() {
        data date = new data();
        data.le_data();
        data.le_hora();
//        if (data.dia.equals("1")) {
//            if ((data.mes.equals("01")) || (data.mes.equals("06"))) {
//                deleteDir(new File(System.getProperty("user.home") + "\\BackupPCB"));
//            }
//        }
        File pasta_backup = new File(System.getProperty("user.home") + "\\BackupPCB\\");
        if (!pasta_backup.exists()) {
            pasta_backup.mkdirs(); //mkdir() cria somente um diretório, mkdirs() cria diretórios e subdiretórios.
        }

        //--------------PASTA DO BACKUP
        File soure = new File("" + System.getProperty("user.dir") + "\\Programa_Biblioteca.mv.db");
        File destination = new File("" + System.getProperty("user.home") + "\\BackupPCB\\Programa_Biblioteca -- " + date.ano + "-" + date.mes + "-" + date.dia + ".mv.db");
        try {
            copyFile(soure, destination);
        } catch (Exception e) {
        }
        //---------------------------
    }

    private static void copyFile(File source, File destination) throws IOException {
        if (destination.exists()) {
            destination.delete();
        }
        FileChannel sourceChannel = null;
        FileChannel destinationChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destinationChannel = new FileOutputStream(destination).getChannel();
            sourceChannel.transferTo(0, sourceChannel.size(),
                    destinationChannel);
        } finally {
            if (sourceChannel != null && sourceChannel.isOpen()) {
                sourceChannel.close();
            }
            if (destinationChannel != null && destinationChannel.isOpen()) {
                destinationChannel.close();
            }
        }
    }
// Deletes all files and subdirectories under dir.
// Returns true if all deletions were successful.
// If a deletion fails, the method stops attempting to delete and returns false.

    private static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // The directory is now empty so delete it
        return dir.delete();
    }
}
