// *3@lî@f;~8+=-2)as,32!$#@&!@#*(()()|_\\/*<g5$1ghj45#000-()()|~6*!@}$#'?ghj45_\\/*fC|/L+=-2)as,32&$(s4}$#'?+=-2)2w\`%_\\/*TdEa8_\\/*ybdfe32dsw#6*dUybdfeybdfe#6*dU9)7tg*7@6&ybdfe!'a**
// "1-nirA/ tg l21goRe03e on1/sHdc51 era" -> 6
package programabiblioteca;

import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;

/**
 *
 * @author Alencar
 */
public class Retirada_Livro extends javax.swing.JFrame {

    public Retirada_Livro() { // Na inicialização estou centralizando a janela, e adicionando icones aos botões
        initComponents();
        this.setIconImage(new ImageIcon("ico\\iconeretiradaDEV.png").getImage());
        setAcessibilidade();
        centralizarComponente();
        jButton1.setIcon(new javax.swing.ImageIcon("ico\\iconeretirada1.png"));
        jButton2.setIcon(new javax.swing.ImageIcon("ico\\iconedevolução.png"));  // http://br.freepik.com/icones-gratis/ponto-seta-curvada-para-baixo_695368.htm
    }

    public Retirada_Livro(String tipo, String id, String id2) { // Na inicialização estou centralizando a janela, e adicionando icones aos botões
        initComponents();
        this.setIconImage(new ImageIcon("ico\\iconeretiradaDEV.png").getImage());
        setAcessibilidade();
        centralizarComponente();
        jButton1.setIcon(new javax.swing.ImageIcon("ico\\iconeretirada1.png"));
        jButton2.setIcon(new javax.swing.ImageIcon("ico\\iconedevolução.png"));  // http://br.freepik.com/icones-gratis/ponto-seta-curvada-para-baixo_695368.htm
        switch (tipo) {
            case "alunos":
                if (id.length() > 0) {
                    J_cod_alu.setText(id);
                }
                J_cod_li.setText(id2);
                break;
            case "livros":
                if (id.length() > 0) {
                    J_cod_li.setText(id);
                }
                J_cod_alu.setText(id2);
                break;
        }
        lost_focus_id_aluno();
        lost_focus_id_livro();
    }

    private void centralizarComponente() { // função para centralizar a janela
        Dimension ds = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dw = getSize();
        setLocation((ds.width - dw.width) / 2, (ds.height - dw.height) / 2);
    }

    public void limpa() { // limpa todos os campos
        J_cod_alu.setText("");
        J_cod_li.setText("");
        jTextField1.setText("");
        jTextField2.setText("");
        jTextField4.setText("");
        jTextField5.setText("");
        jTextField6.setText("");
        J_dias.setText("");
    }

    private void setAcessibilidade() {
        JRootPane meurootpane = getRootPane();
        meurootpane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE");
        meurootpane.getRootPane().getActionMap().put("ESCAPE", new AbstractAction("ESCAPE") {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    public void Retira() {
        // insert into reti_devo (id_aluno, id_livro ) values (1,1);
        if (!J_cod_li.getText().equals("") && !J_cod_alu.getText().equals("")) { // Verifica se foi digitado algum código nos campos necessário
            // caso contrário não irá tentar fazer os comandos abaixo
            boolean VF = true;
            try {
                int t0 = Integer.parseInt(J_cod_li.getText()); // este try-catch somente é para testar se foi digitado somente números,
                int t1 = Integer.parseInt(J_dias.getText());// para assim evitar uma serie de erros
                int t2 = Integer.parseInt(J_cod_alu.getText());

                if (VF) {
                    if (!newBanco.procura_cod_livro(J_cod_li.getText())) { //Verifica se o livro existe
                        if (!newBanco.verifica_cod_aluno(J_cod_alu.getText())) {
                            if (!newBanco.livro_retirado_N(J_cod_li.getText())) { // Verifica se o livro está retirado
                                data data = new data();
                                String data_retirada = data.obtemDataPadrao();
                                String dias = J_dias.getText();
                                String comando = "insert into reti_devo (id_aluno, id_livro, data, dias) values (" + J_cod_alu.getText() + " , " + J_cod_li.getText() + ", '" + data_retirada + "'," + dias + ")";

                                newBanco.insere(comando); // executa o comando
                                JOptionPane.showMessageDialog(null, "Livro Retirado com Sucesso!");
                                newBanco.geraLog(J_cod_alu.getText(), J_cod_li.getText());
                                limpa();
                                J_cod_li.requestFocus();
                            } else {
                                JOptionPane.showMessageDialog(null, "Livro Já Retirado!");
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "Aluno Não Encotrado! Verifique se o código foi digitado corretamente!");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Livro Não Encotrado! Verifique se o código foi digitado corretamente!");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Os Códigos deve ser digitados!");
                }
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha todos os campos corretamente.");
            }
        }
    }

    public void devolve() {
        if (!J_cod_li.getText().equals("") && !J_cod_alu.getText().equals("")) { //verifica se o código do livro foi preenchido, poss para a devolção só é necessário o código do livro
            boolean VF = true;
            try {
                int t = Integer.parseInt(J_cod_li.getText()); // este try-catch somente é para testar se foi digitado somente números,
                // para assim evitar uma serie de erros
            } catch (Exception e) {
                VF = false;
                JOptionPane.showMessageDialog(null, "Somente Números são permitidos no código");
            }
            if (VF) {
                if (newBanco.livro_retirado_N(J_cod_li.getText())) {
                    try {
                        //verifica se o livro está realmente retirado, para somente depois efetuar a devolução
                        ResultSet info_reti_livro = newBanco.pegaDadosNoBD("select * from reti_devo where id_livro = " + J_cod_li.getText());
                        info_reti_livro.first();
                        if ((info_reti_livro.getString("ID_ALUNO")).equals(J_cod_alu.getText())) {
                            if ((Integer.parseInt(newBanco.pega_num_dias(0, J_cod_li.getText()))) < (dias_retirados())) {
                                JOptionPane.showMessageDialog(null, "Este livro está atrasado por " + ((dias_retirados()) - (Integer.parseInt(newBanco.pega_num_dias(0, J_cod_li.getText())))) + " dias");
                            }
                            try {
                                String comando = "delete from reti_devo where id_livro = " + J_cod_li.getText();
                                newBanco.insere(comando);
                                JOptionPane.showMessageDialog(null, "Devolução Concluída!");
                                int a = (int) (0 + Math.random() * 1000);
                                a *= -1;
                                newBanco.geraLog("" + a, J_cod_li.getText());
                                limpa();
                                J_cod_li.requestFocus();

                            } catch (HeadlessException e) {
                                JOptionPane.showMessageDialog(null, e + "\nRetiradaLivro.devolve");
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "Código do aluno não confere com o código de retirada.");
                        }
                    } catch (SQLException ex) {

                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Este Livro não está retirado!");
                }
            }
        }
    }

    public int dias_retirados() {
        int num_dias = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date1 = sdf.parse(newBanco.pega_num_dias(1, J_cod_li.getText()));
            
            data.le_data();
            Date date2 = sdf.parse(data.dia + "/" + data.mes + "/" + data.ano);

            long differenceMilliSeconds = date2.getTime() - date1.getTime();

            num_dias = (int) (differenceMilliSeconds / 1000 / 60 / 60 / 24);
        } catch (ParseException e) {
        }
        return num_dias;
    }

    private void lost_focus_id_livro() {
        if (!J_cod_li.equals("")) { //verifica se o código foi digitado
            boolean VF = true;
            try {
                int t = Integer.parseInt(J_cod_li.getText());// este try-catch somente é para testar se foi digitado somente números,
                // para assim evitar uma serie de erros
            } catch (Exception e) {
                VF = false;
            }
            if (VF) {
                if (!J_cod_li.getText().equals("")) { //verifica se foi digitado alguma coisa
                    try {
                        ResultSet rs = newBanco.pegaDadosNoBD("SELECT * FROM LIVROS WHERE CODIGO = " + J_cod_li.getText() + "");
                        while (rs.next()) {                         //pega as informações do livro e do autor e exibe na hora da devolução
                            jTextField1.setText(rs.getString("NOME_LI"));
                            jTextField2.setText(rs.getString("AUTOR_LI"));

                        }
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, "ERRO: " + ex + "\nRetirada_Livro.lostFocusIdLivro");
                    }
                }
            }
        }
    }

    private void lost_focus_id_aluno() {
        if (!J_cod_alu.equals("")) {
            boolean VF = true;
            try {
                int t = Integer.parseInt(J_cod_alu.getText());// este try-catch somente é para testar se foi digitado somente números,
                // para assim evitar uma serie de erros
            } catch (Exception e) {
                VF = false;
            }
            if (VF) {
                if (!newBanco.verifica_cod_aluno(J_cod_alu.getText())) {    //verifica se o código do aluno existe

                    try {
                        ResultSet rs = newBanco.pegaDadosNoBD("SELECT * FROM ALUNOS WHERE ID = '" + J_cod_alu.getText() + "'");
                        while (rs.next()) {
                            jTextField4.setText(rs.getString("NOME_ALU"));
                            jTextField5.setText(newBanco.pega_nome_turma_turno(Integer.parseInt(rs.getString("TURMA_ALU")), "TURMAS"));
                            jTextField6.setText(newBanco.pega_nome_turma_turno(Integer.parseInt(rs.getString("TURNO_ALU")), "TURNO"));

                        }
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null, "ERRO: " + ex + "\nRetirada_Livro.lostFocusIdAluno");
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jToolBar1 = new javax.swing.JToolBar();
        jLabel3 = new javax.swing.JLabel();
        J_cod_li = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        J_cod_alu = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        J_dias = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Retiradas");
        setResizable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Informações do Livro"));

        jLabel1.setText("Nome:");

        jLabel2.setText("Autor:");

        jTextField1.setEditable(false);
        jTextField1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jTextField1.setFocusable(false);
        jTextField1.setRequestFocusEnabled(false);

        jTextField2.setEditable(false);
        jTextField2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jTextField2.setFocusable(false);
        jTextField2.setRequestFocusEnabled(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField1)
                    .addComponent(jTextField2, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        jLabel3.setText("Cód. Livro:");

        J_cod_li.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                J_cod_liFocusLost(evt);
            }
        });
        J_cod_li.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                J_cod_liKeyPressed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Informações do Aluno"));

        jLabel4.setText("Nome:");

        jTextField4.setEditable(false);
        jTextField4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jTextField4.setFocusable(false);
        jTextField4.setRequestFocusEnabled(false);
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });

        jLabel5.setText("Turma:");

        jTextField5.setEditable(false);
        jTextField5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jTextField5.setFocusable(false);
        jTextField5.setRequestFocusEnabled(false);

        jTextField6.setEditable(false);
        jTextField6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jTextField6.setFocusable(false);
        jTextField6.setRequestFocusEnabled(false);

        jLabel6.setText("Turno:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jTextField6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                        .addComponent(jTextField5, javax.swing.GroupLayout.Alignment.LEADING)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addContainerGap(41, Short.MAX_VALUE))
        );

        jLabel7.setText("Cód. Aluno:");

        J_cod_alu.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                J_cod_aluFocusLost(evt);
            }
        });
        J_cod_alu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                J_cod_aluKeyPressed(evt);
            }
        });

        jButton1.setText("Retirada");
        jButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton2.setText("Devolução");
        jButton2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jButton2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton2KeyPressed(evt);
            }
        });

        J_dias.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                J_diasKeyPressed(evt);
            }
        });

        jLabel8.setText("Dias:");

        jButton3.setText("...");
        jButton3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton5.setText("...");
        jButton5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(J_cod_li, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(3, 3, 3)
                            .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(J_cod_alu, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(36, 36, 36)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(J_dias, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(J_cod_li, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(J_cod_alu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(J_dias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(jButton5)
                    .addComponent(jButton3))
                .addGap(38, 38, 38)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed

    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Retira();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        devolve();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void J_cod_liFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_J_cod_liFocusLost
        lost_focus_id_livro();
    }//GEN-LAST:event_J_cod_liFocusLost

    private void J_cod_aluFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_J_cod_aluFocusLost
        lost_focus_id_aluno();
    }//GEN-LAST:event_J_cod_aluFocusLost

    private void J_cod_liKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_J_cod_liKeyPressed
        if (evt.getKeyCode() == 10) {
            J_cod_alu.requestFocus();
        }
    }//GEN-LAST:event_J_cod_liKeyPressed

    private void J_cod_aluKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_J_cod_aluKeyPressed
        if (evt.getKeyCode() == 10) {
            J_dias.requestFocus();
        }
    }//GEN-LAST:event_J_cod_aluKeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        if (evt.getKeyCode() == 10) {
            Retira();
        }
    }//GEN-LAST:event_jButton1KeyPressed

    private void jButton2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton2KeyPressed
        if (evt.getKeyCode() == 10) {
            devolve();
        }
    }//GEN-LAST:event_jButton2KeyPressed

    private void J_diasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_J_diasKeyPressed
        if (evt.getKeyCode() == 10) {
            jButton1.requestFocus();
        }
    }//GEN-LAST:event_J_diasKeyPressed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        new Alunos_Procurar("retirada", J_cod_li.getText()).setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        new Livros_Procurar("retirada", J_cod_alu.getText()).setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton5ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField J_cod_alu;
    private javax.swing.JTextField J_cod_li;
    private javax.swing.JTextField J_dias;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables
}
