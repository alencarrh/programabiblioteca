/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programabiblioteca;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Alencar
 */
public class DB {

    //create table discipline (ID int auto_increment primary key, name varchar(50), WEEKDAYS varchar(30))
    private final String user;
    private final String password;
    private final String nameClass;
    private final String url;
    private Connection conn;
    private Statement stmt;
    private ResultSet rs;

    public DB(String user, String password, String nameClass, String url) {
        this.user = user;
        this.password = password;
        this.nameClass = nameClass;
        this.url = url;
        this.conn = null;
        this.stmt = null;
        this.rs = null;
        connect();
    }

    private boolean connect() {
        try {
            Class.forName(this.nameClass);
            this.conn = DriverManager.getConnection(this.url, this.user, this.password);
            this.stmt = this.conn.createStatement();
            return true;
        } catch (ClassNotFoundException | SQLException e) {
//            Util.writeLog("There was a problem when connecting with database. >>>  problem: " + e);
            System.out.println("There was a problem when connecting with database. >>>  problem: " + e);
            return false;
        }
    }

    public boolean isConnected() {
        return conn != null;
    }

    public boolean disconnect() {
        try {
            if (conn != null) {
                this.conn.close();
            }
            this.conn = null;
            this.stmt = null;
            this.rs = null;
            return true;
        } catch (Exception e) {
//            Util.writeLog("There was a problem when to try disconnect of database. >>> problem: " + e);
            System.out.println("There was a problem when to try disconnect of database. >>> problem: " + e);
            return false;
        }
    }

    public boolean insereComando(String cmd) {
        try {
            if (this.conn == null) {
                connect();
            }
            this.stmt.executeUpdate(cmd);
            return true;
        } catch (Exception e) {
//            Util.writeLog("There was a problem to execute the command \"" + cmd + "\". >>>  problem: " + e);
            System.out.println("There was a problem to execute the command \"" + cmd + "\". >>>  problem: " + e);
            return false;
        }
    }

    public ResultSet getInfoOfDB(String cmd) {
        try {
            if (this.conn == null) {
                connect();
            }
            this.rs = this.stmt.executeQuery(cmd);
            return rs;
        } catch (Exception e) {
//            Util.writeLog("There was a problem to execute the command \"" + cmd + "\". >>>  problem: " + e);
            System.out.println("There was a problem to execute the command \"" + cmd + "\". >>>  problem: " + e);
            return null;
        }
    }

}
