// *3@lî@f;~8+=-2)as,32!$#@&!@#*(()()|_\\/*<g5$1ghj45#000-()()|~6*!@}$#'?ghj45_\\/*fC|/L+=-2)as,32&$(s4}$#'?+=-2)2w\`%_\\/*TdEa8_\\/*ybdfe32dsw#6*dUybdfeybdfe#6*dU9)7tg*7@6&ybdfe!'a**
// "1-nirA/ tg l21goRe03e on1/sHdc51 era" -> 6
package programabiblioteca;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;

/**
 *
 * @author Alencar
 */
public class Livros_Consultar extends javax.swing.JFrame {

    public Livros_Consultar(String ID) {
        initComponents();
        this.setIconImage(new ImageIcon("ico\\iconelivros2.png").getImage());
        setAcessibilidade();
        centralizarComponente();
        B_SalvarAlterações.setIcon(new javax.swing.ImageIcon("ico\\iconesalvar.png"));
        B_DeletarCadastro.setIcon(new javax.swing.ImageIcon("ico\\iconeeliminar.png"));
        B_Buscar2.setIcon(new javax.swing.ImageIcon("ico\\iconeconsulta.png"));
        J_Cod.setText(ID);
        pega_info(ID);
        ve_aluno();
    }

    public Livros_Consultar(String ID, String Nome, String Autor) {
        initComponents();
        this.setIconImage(new ImageIcon("ico\\iconelivros2.png").getImage());
        setAcessibilidade();
        centralizarComponente();
        B_SalvarAlterações.setIcon(new javax.swing.ImageIcon("ico\\iconesalvar.png"));
        B_DeletarCadastro.setIcon(new javax.swing.ImageIcon("ico\\iconeeliminar.png"));
        B_Buscar2.setIcon(new javax.swing.ImageIcon("ico\\iconeconsulta.png"));
        J_Cod.setText(ID);
        J_Nome_Livro.setText(Nome);
        J_Nome_Autor.setText(Autor);
        ve_aluno();
    }

    public Livros_Consultar() {
        initComponents();
        setAcessibilidade();
        centralizarComponente();
        this.setIconImage(new ImageIcon("ico\\iconelivros2.png").getImage());
        B_SalvarAlterações.setIcon(new javax.swing.ImageIcon("ico\\iconesalvar.png"));
        B_DeletarCadastro.setIcon(new javax.swing.ImageIcon("ico\\iconeeliminar.png"));
        B_Buscar2.setIcon(new javax.swing.ImageIcon("ico\\iconeconsulta.png"));
    }

    private void centralizarComponente() {
        Dimension ds = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dw = getSize();
        setLocation((ds.width - dw.width) / 2, (ds.height - dw.height) / 2);
    }

    private void setAcessibilidade() {
        JRootPane meurootpane = getRootPane();
        meurootpane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE");
        meurootpane.getRootPane().getActionMap().put("ESCAPE", new AbstractAction("ESCAPE") {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    private void ve_aluno() {
        String SIM_NAO = "SIM", cod_alu = null;
        try {
            String cmd = "SELECT HR_ALUNO, HR_LIVRO, AL_NOME, AL_ID "
                    + "FROM HISTORICO_RECENTE "
                    + "INNER JOIN alunos "
                    + "ON HR_ALUNO = AL_ID "
                    + "WHERE HR_LIVRO = " + J_Cod.getText();
            ResultSet rs = newBanco.pegaDadosNoBD(cmd);
            if (rs.next()) {
                SIM_NAO = "NÃO";
                jTextField3.setText(rs.getString("AL_NOME"));
                jTextField4.setText(rs.getString("AL_ID"));
            }
            jTextField2.setText(SIM_NAO);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nLivros_Consultar - 127");
        }
    }

    private void botao_deleta() {
        Cript cri = new Cript();
        cri.valida();
        if (!J_Cod.getText().equals("")) {
            int escolha = JOptionPane.showConfirmDialog(null, "Deseja Realmente Romover Este Cadastro!");
            if (escolha == 0) {
                if (!newBanco.livro_retirado_N(J_Cod.getText())) {
                    String comando = "delete from LIVROS where LI_CODIGO = " + J_Cod.getText() + "";
                    newBanco.insere(comando);
                    JOptionPane.showMessageDialog(null, "Cadastro Deletado!");
                    J_Cod.setText("");
                    J_Nome_Autor.setText("");
                    J_Nome_Livro.setText("");
                    jTextField2.setText("SIM/NÃO");
                    jTextField3.setText("NINGUÉM");
                } else {
                    JOptionPane.showMessageDialog(null, "Este livro está retido e não pode ter seu cadastro excluido!");
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        J_Nome_Livro = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        J_Nome_Autor = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        J_Cod = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        jLabel5 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jTextField1 = new javax.swing.JTextField();
        B_Buscar2 = new javax.swing.JButton();
        B_SalvarAlterações = new javax.swing.JButton();
        B_DeletarCadastro = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Consulta de Livros");
        setResizable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Informações"));

        jLabel1.setText("Nome do Livro: ");

        J_Nome_Livro.setEditable(false);
        J_Nome_Livro.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        J_Nome_Livro.setFocusable(false);

        jLabel2.setText("Nome do Autor:");

        J_Nome_Autor.setEditable(false);
        J_Nome_Autor.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        J_Nome_Autor.setFocusable(false);
        J_Nome_Autor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                J_Nome_AutorActionPerformed(evt);
            }
        });

        jLabel3.setText("Código do Livro:");

        J_Cod.setEditable(false);
        J_Cod.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        J_Cod.setFocusable(false);

        jLabel6.setText("Disponível: ");

        jTextField2.setEditable(false);
        jTextField2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jTextField2.setText("SIM/NÃO");
        jTextField2.setFocusable(false);
        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jLabel7.setText("Retirado por: ");

        jTextField3.setEditable(false);
        jTextField3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jTextField3.setText("NINGUÉM");
        jTextField3.setFocusable(false);

        jTextField4.setEditable(false);
        jTextField4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        jLabel4.setText("Código Aluno:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField3))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 118, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(J_Nome_Livro))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(J_Cod, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(J_Nome_Autor))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(J_Cod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(J_Nome_Livro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(J_Nome_Autor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38))
        );

        jToolBar1.setFloatable(false);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Pesquisar por: ");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Código", "Livro", "Autor" }));

        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        B_Buscar2.setText("Buscar");
        B_Buscar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_Buscar2ActionPerformed(evt);
            }
        });
        B_Buscar2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B_Buscar2KeyPressed(evt);
            }
        });

        B_SalvarAlterações.setText("Editar Cadastro");
        B_SalvarAlterações.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_SalvarAlteraçõesActionPerformed(evt);
            }
        });
        B_SalvarAlterações.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B_SalvarAlteraçõesKeyPressed(evt);
            }
        });

        B_DeletarCadastro.setText("Deletar Cadastro");
        B_DeletarCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_DeletarCadastroActionPerformed(evt);
            }
        });
        B_DeletarCadastro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B_DeletarCadastroKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 457, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(B_Buscar2))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(B_SalvarAlterações, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(B_DeletarCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B_Buscar2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(B_SalvarAlterações, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B_DeletarCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 11, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void B_Buscar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_Buscar2ActionPerformed
        new Livros_Procurar(jComboBox1.getSelectedIndex(), jTextField1.getText().toUpperCase()).setVisible(true);
        dispose();
    }//GEN-LAST:event_B_Buscar2ActionPerformed

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed

    }//GEN-LAST:event_jTextField2ActionPerformed

    private void B_SalvarAlteraçõesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_SalvarAlteraçõesActionPerformed
        if (!J_Cod.getText().equals("")) {
            new Livros_Edita(J_Cod.getText()).setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_B_SalvarAlteraçõesActionPerformed

    private void B_DeletarCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_DeletarCadastroActionPerformed
        botao_deleta();
    }//GEN-LAST:event_B_DeletarCadastroActionPerformed

    private void B_Buscar2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B_Buscar2KeyPressed
        if (evt.getKeyCode() == 10) {
            new Livros_Procurar(jComboBox1.getSelectedIndex(), jTextField1.getText().toUpperCase()).setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_B_Buscar2KeyPressed

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        if (evt.getKeyCode() == 10) {
            new Livros_Procurar(jComboBox1.getSelectedIndex(), jTextField1.getText().toUpperCase()).setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void B_SalvarAlteraçõesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B_SalvarAlteraçõesKeyPressed
        if (!J_Cod.getText().equals("")) {
            new Livros_Edita(J_Cod.getText()).setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_B_SalvarAlteraçõesKeyPressed

    private void B_DeletarCadastroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B_DeletarCadastroKeyPressed

    }//GEN-LAST:event_B_DeletarCadastroKeyPressed

    private void J_Nome_AutorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_J_Nome_AutorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_J_Nome_AutorActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton B_Buscar2;
    private javax.swing.JButton B_DeletarCadastro;
    private javax.swing.JButton B_SalvarAlterações;
    private javax.swing.JTextField J_Cod;
    private javax.swing.JTextField J_Nome_Autor;
    private javax.swing.JTextField J_Nome_Livro;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables

    private void pega_info(String ID) {
        try {
            String cmd = "select * from LIVROS where li_CODIGO = " + ID;
            ResultSet rs = newBanco.pegaDadosNoBD(cmd);
            while (rs.next()) {
                J_Cod.setText(rs.getString("LI_CODIGO"));
                J_Nome_Livro.setText(rs.getString("LI_NOME"));
                J_Nome_Autor.setText(rs.getString("LI_AUTOR"));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nLivros_Consultar.pegaInfo");
        }
    }

}
