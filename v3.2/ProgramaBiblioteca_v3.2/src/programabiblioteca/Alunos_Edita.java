// *3@lî@f;~8+=-2)as,32!$#@&!@#*(()()|_\\/*<g5$1ghj45#000-()()|~6*!@}$#'?ghj45_\\/*fC|/L+=-2)as,32&$(s4}$#'?+=-2)2w\`%_\\/*TdEa8_\\/*ybdfe32dsw#6*dUybdfeybdfe#6*dU9)7tg*7@6&ybdfe!'a**
// "1-nirA/ tg l21goRe03e on1/sHdc51 era" -> 6
package programabiblioteca;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;

/**
 *
 * @author Alencar
 */
public class Alunos_Edita extends javax.swing.JFrame {

    String sexo = "", nome_, turma_, turno_;
    int IDID = 0;

    public Alunos_Edita(String ID) {
        initComponents();
        this.setIconImage(new ImageIcon("ico\\iconealunos2.png").getImage());
        setAcessibilidade();
        centralizarComponente();
        B_SalvarAlterações.setIcon(new javax.swing.ImageIcon("ico\\iconesalvar.png"));
        B_Canela_Volta.setIcon(new javax.swing.ImageIcon("ico\\iconevoltar.png"));
        try {
            ResultSet rs = newBanco.pegaDadosNoBD("SELECT * FROM TURMAS");
            while (rs.next()) {
                J_Turma.addItem(rs.getString("TA_NAME"));
            }
            rs = newBanco.pegaDadosNoBD("SELECT * FROM TURNOS ");
            while (rs.next()) {
                J_Turno.addItem(rs.getString("TO_NAME"));
            }
            rs.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nAlunos_Edita.AlunosEdita(String id)");
        }
        seta_info(ID);
    }

    public Alunos_Edita() {
        initComponents();
        this.setIconImage(new ImageIcon("ico\\iconealunos2.png").getImage());
        centralizarComponente();
        setAcessibilidade();
        B_SalvarAlterações.setIcon(new javax.swing.ImageIcon("ico\\iconesalvar.png"));
        B_Canela_Volta.setIcon(new javax.swing.ImageIcon("ico\\iconevoltar.png"));

    }

    private void setAcessibilidade() {
        JRootPane meurootpane = getRootPane();
        meurootpane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE");
        meurootpane.getRootPane().getActionMap().put("ESCAPE", new AbstractAction("ESCAPE") {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    private void centralizarComponente() {
        Dimension ds = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dw = getSize();
        setLocation((ds.width - dw.width) / 2, (ds.height - dw.height) / 2);
    }

    public void seta_info(String ID) {
        try {
            String cmd = "SELECT al_id, al_nome, al_sexo, ta_name, to_name FROM alunos INNER JOIN turmas ON alunos.al_turma = turmas.ta_id INNER JOIN turnos ON alunos.al_turno = turnos.to_id ";
            ResultSet rs = newBanco.pegaDadosNoBD(cmd + "where AL_ID = " + ID);

            rs.next();

            IDID = Integer.parseInt(rs.getString("AL_ID"));
            nome_ = rs.getString("AL_NOME");
            J_Nome_Completo.setText(nome_);
            turma_ = rs.getString("ta_name");
            J_Turma.setSelectedItem(rs.getString("ta_name"));
            turno_ = rs.getString("to_name");
            J_Turno.setSelectedItem(rs.getString("to_name"));
            if (rs.getString("al_sexo").equals("M")) {
                B_M2.setSelected(true);
                B_F2.setSelected(false);
                sexo = "M";
            } else {
                B_M2.setSelected(false);
                B_F2.setSelected(true);
                sexo = "F";
            }
            rs.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nAlunos_Edita - 140");
        }
    }

    public void salva_() {
        if (!J_Nome_Completo.getText().equals("")) {
            if (!J_Turma.getSelectedItem().equals("Selecione")) {
                if (!J_Turno.getSelectedItem().equals("Selecione")) {
                    if (!sexo.equals("Não")) {
                        int id_turma = 0, id_turno = 0;
                        String nomeTurma = J_Turma.getSelectedItem().toString();
                        String nomeTurno = J_Turno.getSelectedItem().toString();
                        id_turma = newBanco.pega_id_turma_turno(0, nomeTurma);
                        id_turno = newBanco.pega_id_turma_turno(1, nomeTurno);
                        String comando = "UPDATE alunos SET al_nome = '" + J_Nome_Completo.getText().toUpperCase() + "', al_turma = '" + id_turma + "', al_turno = '" + id_turno + "', al_sexo = '" + sexo + "' WHERE al_id = " + IDID;
                        newBanco.insere(comando);
                        new Alunos_Consultar(IDID + "", J_Nome_Completo.getText().toUpperCase(), nomeTurma, nomeTurno).setVisible(true);
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "O sexo deve ser selecionado!");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Selecione um Turno!");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Selecione uma Turma!");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Nome não pode ficar em branco!");
            J_Nome_Completo.requestFocus();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Sexo_Aluno = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        J_Nome_Completo = new javax.swing.JTextField();
        B_M2 = new javax.swing.JRadioButton();
        B_F2 = new javax.swing.JRadioButton();
        jLabel20 = new javax.swing.JLabel();
        J_Turma = new javax.swing.JComboBox();
        J_Turno = new javax.swing.JComboBox();
        jToolBar1 = new javax.swing.JToolBar();
        B_SalvarAlterações = new javax.swing.JButton();
        B_Canela_Volta = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Edita Cadastro");
        setResizable(false);

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setText("Nome Completo: ");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel8.setText("Turma:");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel9.setText("Sexo: ");

        J_Nome_Completo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                J_Nome_CompletoKeyPressed(evt);
            }
        });

        Sexo_Aluno.add(B_M2);
        B_M2.setText("Masculino");
        B_M2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_M2ActionPerformed(evt);
            }
        });

        Sexo_Aluno.add(B_F2);
        B_F2.setText("Feminino");
        B_F2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_F2ActionPerformed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel20.setText("Turno:");

        J_Turma.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione" }));

        J_Turno.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione" }));
        J_Turno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                J_TurnoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(J_Nome_Completo, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(0, 0, 0)
                                .addComponent(B_M2)
                                .addGap(6, 6, 6)
                                .addComponent(B_F2))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(J_Turma, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel20)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(J_Turno, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel7))
                    .addComponent(J_Nome_Completo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(J_Turma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(jLabel9))
                            .addComponent(B_M2)
                            .addComponent(B_F2)))
                    .addComponent(J_Turno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        B_SalvarAlterações.setText("Salvar Alterações");
        B_SalvarAlterações.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_SalvarAlteraçõesActionPerformed(evt);
            }
        });
        B_SalvarAlterações.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B_SalvarAlteraçõesKeyPressed(evt);
            }
        });

        B_Canela_Volta.setText("Voltar");
        B_Canela_Volta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_Canela_VoltaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addComponent(B_SalvarAlterações)
                .addGap(37, 37, 37)
                .addComponent(B_Canela_Volta, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(B_Canela_Volta, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B_SalvarAlterações, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void B_M2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_M2ActionPerformed
        sexo = "M";
    }//GEN-LAST:event_B_M2ActionPerformed

    private void B_F2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_F2ActionPerformed
        sexo = "F";
    }//GEN-LAST:event_B_F2ActionPerformed

    private void B_Canela_VoltaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_Canela_VoltaActionPerformed
        new Alunos_Consultar(IDID + "", nome_, turma_, turno_).setVisible(true);
        dispose();
    }//GEN-LAST:event_B_Canela_VoltaActionPerformed

    private void B_SalvarAlteraçõesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_SalvarAlteraçõesActionPerformed
        salva_();

    }//GEN-LAST:event_B_SalvarAlteraçõesActionPerformed

    private void J_Nome_CompletoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_J_Nome_CompletoKeyPressed
        if (evt.getKeyCode() == 10) {
            salva_();
        }
    }//GEN-LAST:event_J_Nome_CompletoKeyPressed

    private void B_SalvarAlteraçõesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B_SalvarAlteraçõesKeyPressed
        if (evt.getKeyCode() == 10) {
            salva_();
        }
    }//GEN-LAST:event_B_SalvarAlteraçõesKeyPressed

    private void J_TurnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_J_TurnoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_J_TurnoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton B_Canela_Volta;
    private javax.swing.JRadioButton B_F2;
    private javax.swing.JRadioButton B_M2;
    private javax.swing.JButton B_SalvarAlterações;
    private javax.swing.JTextField J_Nome_Completo;
    private javax.swing.JComboBox J_Turma;
    private javax.swing.JComboBox J_Turno;
    private javax.swing.ButtonGroup Sexo_Aluno;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables
}
