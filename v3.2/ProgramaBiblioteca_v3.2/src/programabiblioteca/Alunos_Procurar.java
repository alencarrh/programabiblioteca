// *3@lî@f;~8+=-2)as,32!$#@&!@#*(()()|_\\/*<g5$1ghj45#000-()()|~6*!@}$#'?ghj45_\\/*fC|/L+=-2)as,32&$(s4}$#'?+=-2)2w\`%_\\/*TdEa8_\\/*ybdfe32dsw#6*dUybdfeybdfe#6*dU9)7tg*7@6&ybdfe!'a**
// "1-nirA/ tg l21goRe03e on1/sHdc51 era" -> 6
package programabiblioteca;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Alencar
 */
public class Alunos_Procurar extends javax.swing.JFrame {

    int num1;
    String txt1, pag_anterior, id_livro, cmd = "";
    Thread roda;

    public Alunos_Procurar(String txt, int num) {
        initComponents();
        pag_anterior = "consultar";
        this.setIconImage(new ImageIcon("ico\\iconealunos2.png").getImage());
        setAcessibilidade();
        centralizarComponente();
        B_Buscar.setIcon(new javax.swing.ImageIcon("ico\\iconeconsulta.png"));
        jTextField1.setText(txt);
        try {
            int a = Integer.parseInt(txt);
        } catch (Exception e) {
            num = 1;
        }
        num1 = num;
        txt1 = txt;
        inicio2(num, txt);

    }

    public Alunos_Procurar(String a, String id_livro) {
        initComponents();
        if (a.equals("retirada")) {
            pag_anterior = "retirada";
            this.id_livro = id_livro;
            this.setIconImage(new ImageIcon("ico\\iconealunos2.png").getImage());
            setAcessibilidade();
            centralizarComponente();
            B_Buscar.setIcon(new javax.swing.ImageIcon("ico\\iconeconsulta.png"));
            inicio2(1, "");
        } else {
            new Retirada_Livro().setVisible(true);
            dispose();
        }
    }

    public Alunos_Procurar() {
        initComponents();
        this.setIconImage(new ImageIcon("ico\\iconealunos2.png").getImage());
        centralizarComponente();
        setAcessibilidade();
        B_Buscar.setIcon(new javax.swing.ImageIcon("ico\\iconeconsulta.png"));
    }

    private void setAcessibilidade() {
        JRootPane meurootpane = getRootPane();
        meurootpane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE");
        meurootpane.getRootPane().getActionMap().put("ESCAPE", new AbstractAction("ESCAPE") {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    private void centralizarComponente() {
        Dimension ds = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dw = getSize();
        setLocation((ds.width - dw.width) / 2, (ds.height - dw.height) / 2);
    }

    private void inicio2(int num, String txt) {
        // num = 0 = Pesquisa por Código
        // num = 1 = Pesquisa por Aluno
        // num = 2 = Pesquisa por Turma
        jTable1.setDefaultRenderer(jTable1.getColumnClass(0), new CellRenderer());
        cmd = "SELECT al_id, al_nome, ta_name, to_name FROM alunos INNER JOIN turmas ON alunos.al_turma = turmas.ta_id INNER JOIN turnos ON alunos.al_turno = turnos.to_id ";
        switch (num) {
            case 0:
                if (jCheckBox1.isSelected()) {
                    cmd += "WHERE AL_ID = '" + txt + "' order by AL_ID";
                } else if (jCheckBox2.isSelected()) {
                    cmd += "WHERE AL_ID = '" + txt + "' order by AL_NOME";
                } else if (jCheckBox3.isSelected()) {
                    cmd += "WHERE AL_ID = '" + txt + "' order by TA_NAME";
                } else if (jCheckBox4.isSelected()) {
                    cmd += "WHERE AL_ID = '" + txt + "' order by TO_NAME";
                } else {
                    cmd += "WHERE AL_ID = '" + txt + "'";
                }
                break;
            case 1:
                if (jCheckBox1.isSelected()) {
                    cmd += "WHERE AL_NOME LIKE '%" + txt + "%' order by AL_ID";
                } else if (jCheckBox2.isSelected()) {
                    cmd += "WHERE AL_NOME LIKE '%" + txt + "%' order by AL_NOME";
                } else if (jCheckBox3.isSelected()) {
                    cmd += "WHERE AL_NOME LIKE '%" + txt + "%' order by TA_NAME";
                } else if (jCheckBox4.isSelected()) {
                    cmd += "WHERE AL_NOME LIKE '%" + txt + "%' order by TO_NAME";
                } else {
                    cmd += "WHERE AL_NOME LIKE '%" + txt + "%'";
                }
                break;
            case 2:
                if (jCheckBox1.isSelected()) {
                    cmd += "WHERE TA_NAME = '" + txt + "' order by AL_ID";
                } else if (jCheckBox2.isSelected()) {
                    cmd += "WHERE TA_NAME = '" + txt + "' order by AL_NOME";
                } else if (jCheckBox3.isSelected()) {
                    cmd += "WHERE TA_NAME = '" + txt + "' order by TA_NAME";
                } else if (jCheckBox4.isSelected()) {
                    cmd += "WHERE TA_NAME = '" + txt + "' order by TO_NAME";
                } else {
                    cmd += "WHERE TA_NAME = '" + txt + "'";
                }
                break;
            default:
                break;
        }
        if (roda == null) {
            roda = new roda();
            roda.start();
        }
    }

    public void botao_busca() {
        int num = jComboBox1.getSelectedIndex();
        if (num == 0) {
            try {
                int t = Integer.parseInt(jTextField1.getText());
            } catch (Exception e) {
                num = 1;
            }
        }
        inicio2(num, jTextField1.getText().toUpperCase());
    }

    private void pega_aluno() {
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        if (jTable1.getSelectedRow() >= 0) {
            switch (pag_anterior) {
                case "consultar":
                    new Alunos_Consultar("" + jTable1.getValueAt(jTable1.getSelectedRow(), 0), "" + jTable1.getValueAt(jTable1.getSelectedRow(), 1), "" + jTable1.getValueAt(jTable1.getSelectedRow(), 2), "" + jTable1.getValueAt(jTable1.getSelectedRow(), 3)).setVisible(true);
                    dispose();
                    break;
                case "retirada":
                    new Retirada_Livro("alunos", "" + jTable1.getValueAt(jTable1.getSelectedRow(), 0), id_livro).setVisible(true);
                    dispose();
                    break;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Favor selecionar uma linha");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Ordenado = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        B_SalvarAlterações = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        B_Buscar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        jCheckBox4 = new javax.swing.JCheckBox();
        jToolBar1 = new javax.swing.JToolBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Procurando Alunos");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setDoubleBuffered(false);

        B_SalvarAlterações.setText("Escolher Selecionado");
        B_SalvarAlterações.setBorder(null);
        B_SalvarAlterações.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_SalvarAlteraçõesActionPerformed(evt);
            }
        });

        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Pesquisar Novamente: ");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Código", "Aluno", "Turma" }));

        B_Buscar.setText("Buscar");
        B_Buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_BuscarActionPerformed(evt);
            }
        });
        B_Buscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B_BuscarKeyPressed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Nome", "Turma", "Turno"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(1);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(300);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(100);
        }

        jLabel2.setText("Ordenar por:");

        Ordenado.add(jCheckBox1);
        jCheckBox1.setSelected(true);
        jCheckBox1.setText("Código");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });

        Ordenado.add(jCheckBox2);
        jCheckBox2.setText("Nome");

        Ordenado.add(jCheckBox3);
        jCheckBox3.setText("Turma");

        Ordenado.add(jCheckBox4);
        jCheckBox4.setText("Turno");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(B_Buscar))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(231, 231, 231)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCheckBox1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jCheckBox2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCheckBox3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCheckBox4)))
                        .addGap(0, 64, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(B_SalvarAlterações, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jCheckBox1)
                    .addComponent(jCheckBox2)
                    .addComponent(jCheckBox3)
                    .addComponent(jCheckBox4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B_Buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(B_SalvarAlterações, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(15, 15, 15))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void B_SalvarAlteraçõesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_SalvarAlteraçõesActionPerformed
        pega_aluno();
    }//GEN-LAST:event_B_SalvarAlteraçõesActionPerformed

    private void B_BuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_BuscarActionPerformed
        botao_busca();
    }//GEN-LAST:event_B_BuscarActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed

    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        if (evt.getKeyCode() == 10) {
            botao_busca();
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void B_BuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B_BuscarKeyPressed
        if (evt.getKeyCode() == 10) {
            botao_busca();
        }
    }//GEN-LAST:event_B_BuscarKeyPressed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        if (evt.getKeyCode() == 10) {
            pega_aluno();
        }
    }//GEN-LAST:event_jTable1KeyPressed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed

    }//GEN-LAST:event_formWindowClosed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton B_Buscar;
    private javax.swing.JButton B_SalvarAlterações;
    private javax.swing.ButtonGroup Ordenado;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JCheckBox jCheckBox4;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables

    class roda extends Thread {

        public void run() {

            try {
                ResultSet rs;
                rs = newBanco.pegaDadosNoBD(cmd);
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                model.setNumRows(0);
                while (rs.next()) {
                    model.addRow(
                            new Object[]{
                                rs.getString("AL_ID"),
                                rs.getString("AL_NOME"),
                                rs.getString("TA_NAME"),
                                rs.getString("TO_NAME")
                            });
                }
                rs.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nAlunos_Procurar.roda");
            }
            roda = null;
        }

    }
}
