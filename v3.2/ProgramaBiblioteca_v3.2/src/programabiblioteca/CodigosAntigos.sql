/**
 * Author:  Alencar Rodrigo Hentges <alencarhentges@gmail.com>
 * Created: 25/07/2016
 */


create table turmas(
    ID int auto_increment primary key, 
    NAME varchar (30) 
);
create table turno(
    ID int auto_increment primary key,
    NAME varchar (30)
);
create table seguranca(
    chave varchar(500)
);
create table alunos( 
    ID int auto_increment primary key, 
    NOME_ALU varchar (55), 
    TURMA_ALU varchar(3), 
    TURNO_ALU varchar(3), 
    SEXO_ALU char(1) 
);
create table livros(
    ID int auto_increment, 
    codigo int primary key,
    NOME_LI varchar(55),
    AUTOR_LI varchar(50)
);
create table reti_devo(
    ID int auto_increment primary key,
    ID_aluno int,
    ID_livro int,
    data varchar(10), 
    dias int
);
create table logs(
    ID int auto_increment primary key,
    ID_ALUNO int,
    ID_LIVRO int,
    DATA varchar(30)
);


