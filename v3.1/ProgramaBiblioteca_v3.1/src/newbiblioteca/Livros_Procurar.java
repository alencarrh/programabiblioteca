// *3@lî@f;~8+=-2)as,32!$#@&!@#*(()()|_\\/*<g5$1ghj45#000-()()|~6*!@}$#'?ghj45_\\/*fC|/L+=-2)as,32&$(s4}$#'?+=-2)2w\`%_\\/*TdEa8_\\/*ybdfe32dsw#6*dUybdfeybdfe#6*dU9)7tg*7@6&ybdfe!'a**
// "1-nirA/ tg l21goRe03e on1/sHdc51 era" -> 6
package newbiblioteca;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Alencar
 */
public class Livros_Procurar extends javax.swing.JFrame {

    String pag_anterior, id_aluno, cmd = "";
    Thread roda;

    public Livros_Procurar(int num, String txt) {
        initComponents();
        pag_anterior = "consultar";
        setAcessibilidade();
        centralizarComponente();
        this.setIconImage(new ImageIcon("ico\\iconelivros2.png").getImage());
        B_Buscar.setIcon(new javax.swing.ImageIcon("ico\\iconeconsulta.png"));
        try {
            int a = Integer.parseInt(txt);
        } catch (Exception e) {
            num = 1;
        }
        jTextField1.setText(txt.toUpperCase());
        inicio2(num, txt);
    }

    public Livros_Procurar(String a, String id_aluno) {
        initComponents();

        if (a.equals("retirada")) {
            pag_anterior = "retirada";
            this.id_aluno = id_aluno;
            setAcessibilidade();
            centralizarComponente();
            this.setIconImage(new ImageIcon("ico\\iconelivros2.png").getImage());
            B_Buscar.setIcon(new javax.swing.ImageIcon("ico\\iconeconsulta.png"));
            inicio2(1, "");
        } else {
            new Retirada_Livro().setVisible(true);
            dispose();
        }
    }

    public Livros_Procurar() {
        initComponents();
        setAcessibilidade();
        centralizarComponente();
        this.setIconImage(new ImageIcon("ico\\iconelivros2.png").getImage());
        B_Buscar.setIcon(new javax.swing.ImageIcon("ico\\iconeconsulta.png"));

    }

    private void setAcessibilidade() {
        JRootPane meurootpane = getRootPane();
        meurootpane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE");
        meurootpane.getRootPane().getActionMap().put("ESCAPE", new AbstractAction("ESCAPE") {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    private void inicio2(int num, String txt) {
        // num = 0 = Pesquisa por Código
        // num = 1 = Pesquisa por Aluno
        // num = 2 = Pesquisa por Turma
        jTable1.setDefaultRenderer(jTable1.getColumnClass(0), new CellRenderer());
        if (num == 0) {
            if (jCheckBox1.isSelected()) {
                cmd = "SELECT * FROM LIVROS WHERE CODIGO = '" + txt + "' order by ID";
            } else if (jCheckBox2.isSelected()) {
                cmd = "SELECT * FROM LIVROS WHERE CODIGO = '" + txt + "' order by NOME_LI";
            } else if (jCheckBox3.isSelected()) {
                cmd = "SELECT * FROM LIVROS WHERE CODIGO = '" + txt + "' order by AUTOR_LI";
            } else {
                cmd = "SELECT * FROM LIVROS WHERE CODIGO = '" + txt + "'";
            }
        } else if (num == 1) {
            if (jCheckBox1.isSelected()) {
                cmd = "SELECT * FROM LIVROS WHERE NOME_LI LIKE '%" + txt + "%' order by ID";
            } else if (jCheckBox2.isSelected()) {
                cmd = "SELECT * FROM LIVROS WHERE NOME_LI LIKE '%" + txt + "%' order by NOME_LI";
            } else if (jCheckBox3.isSelected()) {
                cmd = "SELECT * FROM LIVROS WHERE NOME_LI LIKE '%" + txt + "%' order by AUTOR_LI";
            } else {
                cmd = "SELECT * FROM LIVROS WHERE NOME_LI LIKE '%" + txt + "%'";
            }
        } else if (num == 2) {
            if (jCheckBox1.isSelected()) {
                cmd = "SELECT * FROM LIVROS WHERE AUTOR_LI LIKE '%" + txt + "%' order by ID";
            } else if (jCheckBox2.isSelected()) {
                cmd = "SELECT * FROM LIVROS WHERE AUTOR_LI LIKE '%" + txt + "%' order by NOME_LI";
            } else if (jCheckBox3.isSelected()) {
                cmd = "SELECT * FROM LIVROS WHERE AUTOR_LI LIKE '%" + txt + "%' order by AUTOR_LI";
            } else {
                cmd = "SELECT * FROM LIVROS WHERE AUTOR_LI LIKE '%" + txt + "%'";
            }
        }
        if (roda == null) {
            roda = new roda();
            roda.start();
        }

    }

    private void pega_livro() {
        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
        if (jTable1.getSelectedRow() >= 0) {
            switch (pag_anterior) {
                case "consultar":
                    new Livros_Consultar("" + jTable1.getValueAt(jTable1.getSelectedRow(), 0), "" + jTable1.getValueAt(jTable1.getSelectedRow(), 1), "" + jTable1.getValueAt(jTable1.getSelectedRow(), 2)).setVisible(true);
                    dispose();
                    break;
                case "retirada":
                    new Retirada_Livro("livros", "" + jTable1.getValueAt(jTable1.getSelectedRow(), 0), id_aluno).setVisible(true);
                    dispose();
                    break;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Favor selecionar uma linha");
        }
    }

    private void centralizarComponente() {
        Dimension ds = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dw = getSize();
        setLocation((ds.width - dw.width) / 2, (ds.height - dw.height) / 2);
    }

    private void busca_livro() {
        int num = jComboBox1.getSelectedIndex();
        if (num == 0) {
            try {
                int t = Integer.parseInt(jTextField1.getText());
            } catch (Exception e) {
                num = 1;
            }
        }
        inicio2(num, jTextField1.getText().toUpperCase());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        B_SalvarAlterações = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        B_Buscar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        jToolBar1 = new javax.swing.JToolBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Procurando Livros");

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setDoubleBuffered(false);

        B_SalvarAlterações.setText("Escolher Selecionado");
        B_SalvarAlterações.setBorder(null);
        B_SalvarAlterações.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_SalvarAlteraçõesActionPerformed(evt);
            }
        });

        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Pesquisar Novamente: ");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Código", "Livro", "Autor" }));

        B_Buscar.setText("Buscar");
        B_Buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_BuscarActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Nome", "Autor"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(20);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(250);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(250);
        }

        jLabel2.setText("Ordenar por:");

        buttonGroup1.add(jCheckBox1);
        jCheckBox1.setSelected(true);
        jCheckBox1.setText("Código");

        buttonGroup1.add(jCheckBox2);
        jCheckBox2.setText("Nome");

        buttonGroup1.add(jCheckBox3);
        jCheckBox3.setText("Autor");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(B_Buscar))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(57, 57, 57)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jCheckBox1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCheckBox2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jCheckBox3)))
                        .addGap(0, 214, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(B_SalvarAlterações, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jCheckBox1)
                    .addComponent(jCheckBox2)
                    .addComponent(jCheckBox3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B_Buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 222, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(B_SalvarAlterações, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void B_SalvarAlteraçõesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_SalvarAlteraçõesActionPerformed
        pega_livro();
    }//GEN-LAST:event_B_SalvarAlteraçõesActionPerformed

    private void B_BuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_BuscarActionPerformed
        busca_livro();
    }//GEN-LAST:event_B_BuscarActionPerformed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        if (evt.getKeyCode() == 10) {
            pega_livro();

        }
    }//GEN-LAST:event_jTable1KeyPressed

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        if (evt.getKeyCode() == 10) {
            busca_livro();
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton B_Buscar;
    private javax.swing.JButton B_SalvarAlterações;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables

    class roda extends Thread {

        public void run() {
            try {
                ResultSet rs;
                rs = newBanco.pegaDadosNoBD(cmd);
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                model.setNumRows(0);
                while (rs.next()) {

                    model.addRow(
                            new Object[]{
                                rs.getString("CODIGO"),
                                rs.getString("NOME_LI"),
                                rs.getString("AUTOR_LI")

                            });
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nLivros_Procurar.roda");
            }
            roda = null;
        }
    }
}
