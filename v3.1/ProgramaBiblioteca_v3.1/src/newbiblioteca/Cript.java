// *3@lî@f;~8+=-2)as,32!$#@&!@#*(()()|_\\/*<g5$1ghj45#000-()()|~6*!@}$#'?ghj45_\\/*fC|/L+=-2)as,32&$(s4}$#'?+=-2)2w\`%_\\/*TdEa8_\\/*ybdfe32dsw#6*dUybdfeybdfe#6*dU9)7tg*7@6&ybdfe!'a**
// "1-nirA/ tg l21goRe03e on1/sHdc51 era" -> 6
package newbiblioteca;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Alencar
 */
public class Cript {

    public String pega_codigo() {//Está função irá pegar o código de segurança para efetuar a verificação

        String chave = "";
        try {
            ResultSet rs = newBanco.pegaDadosNoBD("select * from seguranca");
            while (rs.next()) {
                chave = rs.getString("chave");
            }
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nCript.pegaCodigo");
            ex.printStackTrace();
        }
        return chave;
    }

    public void valida() { // aqui nesta parte o método irá validar o código, se for o código certo, não fara nada e voltará para o programa

        try {

            String NSHD = "" + getHDSerial();
            String NSHDC = codifica(NSHD);

            String NSP = "" + getCPUSerial();
            String NSPC = codifica(NSP);

            String NSPM = "" + getMotherboardSerial();
            String NSPMC = codifica(NSPM);

            String CS = "";
            String código;
            try {
                CS = pega_codigo();
            } catch (Exception e) {
                System.exit(0);
            }

            try {
                if (CS.equals(NSHDC)) {
                } else {
                    String comando = "truncate table seguranca";
                    newBanco.insere(comando);
                    JOptionPane.showMessageDialog(null, "Código de Segurança Inválido!\nContate o Responsável pelo programa e peça um novo código");
                    código = JOptionPane.showInputDialog("Digite o Código de Segurança: (Após Digitar o Código abra o programa normalmente)");

                    comando = "insert into seguranca values ('" + código + "')";
                    newBanco.insere(comando);
                    System.exit(0);
                }
            } catch (Exception e) {
                System.exit(0);
            }

        } catch (IOException e) {
        }

    }

    private final static String getHDSerial() throws IOException {
        String os = System.getProperty("os.name");
        try {
            if (os.startsWith("Windows")) {
                return getHDSerialWindows("C");
            } else if (os.startsWith("Linux")) {
                return getHDSerialLinux();
            } else {
                throw new IOException("unknown operating system: " + os);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new IOException(ex.getMessage());
        }
    }

    private final static String getCPUSerial() throws IOException {
        String os = System.getProperty("os.name");

        try {
            if (os.startsWith("Windows")) {
                return getCPUSerialWindows();
            } else if (os.startsWith("Linux")) {
                return getCPUSerialLinux();
            } else {
                throw new IOException("unknown operating system: " + os);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new IOException(ex.getMessage());
        }
    }

    private final static String getMotherboardSerial() throws IOException {
        String os = System.getProperty("os.name");

        try {
            if (os.startsWith("Windows")) {
                return getMotherboardSerialWindows();
            } else if (os.startsWith("Linux")) {
                return getMotherboardSerialLinux();
            } else {
                throw new IOException("unknown operating system: " + os);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new IOException(ex.getMessage());
        }
    }

    // Implementacoes
    /*
     * Captura serial de placa mae no WINDOWS, atraves da execucao de script visual basic
     */
    public static String getMotherboardSerialWindows() {
        String result = "";
        try {
            File file = File.createTempFile("realhowto", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new java.io.FileWriter(file);

            String vbs = "Set objWMIService = GetObject(\"winmgmts:\\\\.\\root\\cimv2\")\n"
                    + "Set colItems = objWMIService.ExecQuery _ \n"
                    + "   (\"Select * from Win32_BaseBoard\") \n"
                    + "For Each objItem in colItems \n"
                    + "    Wscript.Echo objItem.SerialNumber \n"
                    + "    exit for  ' do the first cpu only! \n" + "Next \n";

            fw.write(vbs);
            fw.close();
            Process p = Runtime.getRuntime().exec(
                    "cscript //NoLogo " + file.getPath());
            BufferedReader input = new BufferedReader(new InputStreamReader(p
                    .getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();
        } catch (Exception e) {
        }
        return result.trim();
    }

    /*
     * Captura serial de placa mae em sistemas LINUX, atraves da execucao de comandos em shell.
     */
    public static String getMotherboardSerialLinux() {
        String result = "";
        try {
            String[] args = {"bash", "-c", "lshw -class bus | grep serial"};
            Process p = Runtime.getRuntime().exec(args);
            BufferedReader input = new BufferedReader(new InputStreamReader(p
                    .getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();

        } catch (Exception e) {

        }
        if (result.trim().length() < 1 || result == null) {
            result = "NO_DISK_ID";

        }

        return filtraString(result, "serial: ");

    }

    /*
     * Captura serial de HD no WINDOWS, atraves da execucao de script visual basic
     */
    public static String getHDSerialWindows(String drive) {
        String result = "";
        try {
            File file = File.createTempFile("tmp", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new java.io.FileWriter(file);
            String vbs = "Set objFSO = CreateObject(\"Scripting.FileSystemObject\")\n"
                    + "Set colDrives = objFSO.Drives\n"
                    + "Set objDrive = colDrives.item(\""
                    + drive
                    + "\")\n"
                    + "Wscript.Echo objDrive.SerialNumber";
            fw.write(vbs);
            fw.close();
            Process p = Runtime.getRuntime().exec(
                    "cscript //NoLogo " + file.getPath());
            BufferedReader input = new BufferedReader(new InputStreamReader(p
                    .getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();
        } catch (Exception e) {

        }
        if (result.trim().length() < 1 || result == null) {
            result = "NO_DISK_ID";
        }
        return result.trim();
    }

    /*
     * Captura serial de HD em sistemas Linux, atraves da execucao de comandos em shell.
     */
    public static String getHDSerialLinux() {
        String result = "";
        try {
            String[] args = {"bash", "-c", "lshw -class disk | grep serial"};
            Process p = Runtime.getRuntime().exec(args);
            BufferedReader input = new BufferedReader(new InputStreamReader(p
                    .getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();

        } catch (Exception e) {

        }
        if (result.trim().length() < 1 || result == null) {
            result = "NO_DISK_ID";

        }

        return filtraString(result, "serial: ");

    }

    /*
     * Captura serial da CPU no WINDOWS, atraves da execucao de script visual basic
     */
    public static String getCPUSerialWindows() {
        String result = "";
        try {
            File file = File.createTempFile("tmp", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new java.io.FileWriter(file);

            String vbs = "On Error Resume Next \r\n\r\n"
                    + "strComputer = \".\"  \r\n"
                    + "Set objWMIService = GetObject(\"winmgmts:\" _ \r\n"
                    + "    & \"{impersonationLevel=impersonate}!\\\\\" & strComputer & \"\\root\\cimv2\") \r\n"
                    + "Set colItems = objWMIService.ExecQuery(\"Select * from Win32_Processor\")  \r\n "
                    + "For Each objItem in colItems\r\n "
                    + "    Wscript.Echo objItem.ProcessorId  \r\n "
                    + "    exit for  ' do the first cpu only! \r\n"
                    + "Next                    ";

            fw.write(vbs);
            fw.close();
            Process p = Runtime.getRuntime().exec(
                    "cscript //NoLogo " + file.getPath());
            BufferedReader input = new BufferedReader(new InputStreamReader(p
                    .getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();
        } catch (Exception e) {

        }
        if (result.trim().length() < 1 || result == null) {
            result = "NO_CPU_ID";
        }
        return result.trim();
    }

    /*
     * Captura serial de CPU em sistemas Linux, atraves da execucao de comandos em shell.
     */
    public static String getCPUSerialLinux() {
        String result = "";
        try {
            String[] args = {"bash", "-c", "lshw -class processor | grep serial"};
            Process p = Runtime.getRuntime().exec(args);
            BufferedReader input = new BufferedReader(new InputStreamReader(p
                    .getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();

        } catch (Exception e) {

        }
        if (result.trim().length() < 1 || result == null) {
            result = "NO_DISK_ID";

        }

        return filtraString(result, "serial: ");
    }

    public static String filtraString(String nome, String delimitador) {
        return nome.split(delimitador)[1];
    }

    public String codifica(String a) {
        String resp = "", a2 = a.toLowerCase();

        for (int i = 0; i < a2.length(); i++) {
            switch ("" + a2.charAt(i)) {
                case "a":
                    resp = resp + "!@#*(";
                    break;
                case "á":
                    resp = resp + "ax4%@";
                    break;
                case "â":
                    resp = resp + "ke$*!";
                    break;
                case "ã":
                    resp = resp + "((8&#";
                    break;
                case "à":
                    resp = resp + "k&G3$";
                    break;
                case "b":
                    resp = resp + "178#9";
                    break;
                case "c":
                    resp = resp + "!$#@&";
                    break;
                case "ç":
                    resp = resp + "mnj%*";
                    break;
                case "d":
                    resp = resp + "#000-";
                    break;
                case "e":
                    resp = resp + "+=-2)";
                    break;
                case "ê":
                    resp = resp + "#$!22";
                    break;
                case "é":
                    resp = resp + "@|\"*7";
                    break;
                case "f":
                    resp = resp + "5??{%";
                    break;
                case "g":
                    resp = resp + "}$#D?";
                    break;
                case "h":
                    resp = resp + "\"012|";
                    break;
                case "i":
                    resp = resp + "~6*!@";
                    break;
                case "í":
                    resp = resp + "k%&#s";
                    break;
                case "j":
                    resp = resp + "~^4,7";
                    break;
                case "k":
                    resp = resp + "(_3f7";
                    break;
                case "l":
                    resp = resp + "@f;~8";
                    break;
                case "m":
                    resp = resp + "]eo93";
                    break;
                case "n":
                    resp = resp + "as,32";
                    break;
                case "o":
                    resp = resp + "ghj45";
                    break;
                case "ó":
                    resp = resp + "kHF*&";
                    break;
                case "ô":
                    resp = resp + "!@AW(";
                    break;
                case "õ":
                    resp = resp + "KB$%3";
                    break;
                case "ò":
                    resp = resp + "($7@h";
                    break;
                case "p":
                    resp = resp + "_56#2";
                    break;
                case "q":
                    resp = resp + "pch&6";
                    break;
                case "r":
                    resp = resp + "()()|";
                    break;
                case "s":
                    resp = resp + "2w\\`%";
                    break;
                case "t":
                    resp = resp + "&$(s4";
                    break;
                case "u":
                    resp = resp + "%%=%+";
                    break;
                case "ú":
                    resp = resp + "$3@7*";
                    break;
                case "v":
                    resp = resp + "dfg$*";
                    break;
                case "w":
                    resp = resp + "-341o";
                    break;
                case "x":
                    resp = resp + "=[]~.";
                    break;
                case "y":
                    resp = resp + "%823t";
                    break;
                case "z":
                    resp = resp + "fgt1q";
                    break;
                case "1":
                    resp = resp + "ybdfe";
                    break;
                case "2":
                    resp = resp + "9)7tg";
                    break;
                case "3":
                    resp = resp + "32dsw";
                    break;
                case "4":
                    resp = resp + "@335#";
                    break;
                case "5":
                    resp = resp + "!5a**";
                    break;
                case "6":
                    resp = resp + "44#!*";
                    break;
                case "7":
                    resp = resp + "#@&<>";
                    break;
                case "8":
                    resp = resp + "!-*4@";
                    break;
                case "9":
                    resp = resp + "%8#*@";
                    break;
                case "0":
                    resp = resp + "*7@6&";
                    break;
                case " ":
                    resp = resp + "_\\\\/*";
                    break;
                case "_":
                    resp = resp + "Kj&X/";
                    break;
                case "\\":
                    resp = resp + "85dea";
                    break;
                case "/":
                    resp = resp + "#6*dU";
                    break;
                case "@":
                    resp = resp + "1f2Eg";
                    break;
                case ":":
                    resp = resp + "Twop2";
                    break;
                case ".":
                    resp = resp + "Onep1";
                    break;
                case "-":
                    resp = resp + "TdEa8";
                    break;
                case "!":
                    resp = resp + "Yg584";
                    break;
                case "#":
                    resp = resp + "BB214";
                    break;
                case "$":
                    resp = resp + "*4D2@";
                    break;
                case "%":
                    resp = resp + "##Af2";
                    break;
            }

        }

        return resp;

    }

    public String decodifica(String a) {
        String teste = "", resp = "";
        for (int i = 0; i < a.length() - 4; i++) {
            teste = teste + a.charAt(i) + a.charAt(i + 1) + a.charAt(i + 2) + a.charAt(i + 3) + a.charAt(i + 4);

            switch (teste) {
                case "!@#*(":
                    resp = resp + "a";
                    break;
                case "ax4%@":
                    resp = resp + "á";
                    break;
                case "ke$*!":
                    resp = resp + "â";
                    break;
                case "((8&#":
                    resp = resp + "ã";
                    break;
                case "k&G3$":
                    resp = resp + "à";
                    break;
                case "178#9":
                    resp = resp + "b";
                    break;
                case "!$#@&":
                    resp = resp + "c";
                    break;
                case "mnj%*":
                    resp = resp + "ç";
                    break;
                case "#000-":
                    resp = resp + "d";
                    break;
                case "+=-2)":
                    resp = resp + "e";
                    break;
                case "#$!22":
                    resp = resp + "ê";
                    break;
                case "@|\"*7":
                    resp = resp + "é";
                    break;
                case "5??{%":
                    resp = resp + "f";
                    break;
                case "}$#D?":
                    resp = resp + "g";
                    break;
                case "\"012|":
                    resp = resp + "h";
                    break;
                case "~6*!@":
                    resp = resp + "i";
                    break;
                case "k%&#s":
                    resp = resp + "í";
                    break;
                case "~^4,7":
                    resp = resp + "j";
                    break;
                case "(_3f7":
                    resp = resp + "k";
                    break;
                case "@f;~8":
                    resp = resp + "l";
                    break;
                case "]eo93":
                    resp = resp + "m";
                    break;
                case "as,32":
                    resp = resp + "n";
                    break;
                case "ghj45":
                    resp = resp + "o";
                    break;
                case "kHF*&":
                    resp = resp + "ó";
                    break;
                case "!@AW(":
                    resp = resp + "ô";
                    break;
                case "KB$%3":
                    resp = resp + "õ";
                    break;
                case "($7@h":
                    resp = resp + "ò";
                    break;
                case "_56#2":
                    resp = resp + "p";
                    break;
                case "pch&6":
                    resp = resp + "q";
                    break;
                case "()()|":
                    resp = resp + "r";
                    break;
                case "2w\\`%":
                    resp = resp + "s";
                    break;
                case "&$(s4":
                    resp = resp + "t";
                    break;
                case "%%=%+":
                    resp = resp + "u";
                    break;
                case "$3@7*":
                    resp = resp + "ú";
                    break;
                case "dfg$*":
                    resp = resp + "v";
                    break;
                case "-341o":
                    resp = resp + "w";
                    break;
                case "=[]~.":
                    resp = resp + "x";
                    break;
                case "%823t":
                    resp = resp + "y";
                    break;
                case "fgt1q":
                    resp = resp + "z";
                    break;
                case "ybdfe":
                    resp = resp + "1";
                    break;
                case "9)7tg":
                    resp = resp + "2";
                    break;
                case "32dsw":
                    resp = resp + "3";
                    break;
                case "@335#":
                    resp = resp + "4";
                    break;
                case "!5a**":
                    resp = resp + "5";
                    break;
                case "44#!*":
                    resp = resp + "6";
                    break;
                case "#@&<>":
                    resp = resp + "7";
                    break;
                case "!-*4@":
                    resp = resp + "8";
                    break;
                case "%8#*@":
                    resp = resp + "9";
                    break;
                case "*7@6&":
                    resp = resp + "0";
                    break;
                case "_\\\\/*":
                    resp = resp + " ";
                    break;
                case "Kj&X/":
                    resp = resp + "_";
                    break;
                case "85dea":
                    resp = resp + "\\";
                    break;
                case "#6*dU":
                    resp = resp + "/";
                    break;
                case "1f2Eg":
                    resp = resp + "@";
                    break;
                case "Twop2":
                    resp = resp + ":";
                    break;
                case "Onep1":
                    resp = resp + ".";
                    break;
                case "TdEa8":
                    resp = resp + "-";
                    break;
                case "Yg584":
                    resp = resp + "!";
                    break;
                case "BB214":
                    resp = resp + "#";
                    break;
                case "*4D2@":
                    resp = resp + "";
                    break;
                case "##Af2":
                    resp = resp + "%";
                    break;

            }
            //jTextField1.setText(resp);
            teste = "";
        }
        return resp;

    }
}
