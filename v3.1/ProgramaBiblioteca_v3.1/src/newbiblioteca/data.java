// *3@lî@f;~8+=-2)as,32!$#@&!@#*(()()|_\\/*<g5$1ghj45#000-()()|~6*!@}$#'?ghj45_\\/*fC|/L+=-2)as,32&$(s4}$#'?+=-2)2w\`%_\\/*TdEa8_\\/*ybdfe32dsw#6*dUybdfeybdfe#6*dU9)7tg*7@6&ybdfe!'a**
// "1-nirA/ tg l21goRe03e on1/sHdc51 era" -> 6
package newbiblioteca;

import java.text.SimpleDateFormat;
import java.util.Date;

public class data {

    static String mes, dia, ano, dia_semana, hora, min, seg;
    static SimpleDateFormat horaformatada = new SimpleDateFormat("HH");
    static SimpleDateFormat horaformatada1 = new SimpleDateFormat("mm");
    static SimpleDateFormat horaformatada2 = new SimpleDateFormat("ss");

    
    public static void le_hora() {
        Date horaAtual = new Date();
        hora = horaformatada.format(horaAtual);
        min = horaformatada1.format(horaAtual);
        seg = horaformatada2.format(horaAtual);
    }

    public static String obtemDataPadrao() {
        le_data();
        return dia + "/" + mes + "/" + ano;
    }

    public static String inverteData(String dia, String mes, String ano) {
        return ano + "/" + mes + "/" + dia;
    }

    public static String obtemDataParaNomeArquivo() {
        le_data();
        le_hora();
        return ano + "-" + mes + "-" + dia + " - " + hora + "h" + min + "m" + seg + "s";
    }

    public String obtemDataHoraPadraoReversa() {
        le_data();
        le_hora();
        return ano + "/" + mes + "/" + dia + " - " + hora + "h" + min + "m" + seg + "s";
    }

    public static void le_data() {

        Date data = new Date();
        //mes = ""+data.getMonth();
        dia = "" + data.getDate();
        ano = "" + (1900 + data.getYear());
        dia_semana = "" + data.getDay();
        switch (data.getMonth()) {
            case 0:
                mes = "01";
                break;
            case 1:
                mes = "02";
                break;
            case 2:
                mes = "03";
                break;
            case 3:
                mes = "04";
                break;
            case 4:
                mes = "05";
                break;
            case 5:
                mes = "06";
                break;
            case 6:
                mes = "07";
                break;
            case 7:
                mes = "08";
                break;
            case 8:
                mes = "09";
                break;
            case 9:
                mes = "10";
                break;
            case 10:
                mes = "11";
                break;
            case 11:
                mes = "12";
                break;
        }
    }

}
