// *3@lî@f;~8+=-2)as,32!$#@&!@#*(()()|_\\/*<g5$1ghj45#000-()()|~6*!@}$#'?ghj45_\\/*fC|/L+=-2)as,32&$(s4}$#'?+=-2)2w\`%_\\/*TdEa8_\\/*ybdfe32dsw#6*dUybdfeybdfe#6*dU9)7tg*7@6&ybdfe!'a**
// "1-nirA/ tg l21goRe03e on1/sHdc51 era" -> 6
package newbiblioteca;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;

/**
 *
 * @author Alencar
 */
public final class Alunos_Cadastrar extends javax.swing.JFrame {

    String sexo = "Não";

    public Alunos_Cadastrar() {
        initComponents();
        this.setIconImage(new ImageIcon("ico\\iconealunos2.png").getImage());
        setAcessibilidade();
        centralizarComponente();
        B_Cadastrar.setIcon(new javax.swing.ImageIcon("ico\\iconeconfirma.png"));
        B_Consultar.setIcon(new javax.swing.ImageIcon("ico\\iconeconsulta.png"));
        B_Limpar.setIcon(new javax.swing.ImageIcon("ico\\icone_limpar.png"));
        try {
            inicio();
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Ocorreu um ERRO na tentativa de se conectar com o servidor de dados!" + " \nAlunos_Cadastrar.AlunosCadastrar()");
        }
    }

    public void setAcessibilidade() {
        JRootPane meurootpane = getRootPane();
        meurootpane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE");
        meurootpane.getRootPane().getActionMap().put("ESCAPE", new AbstractAction("ESCAPE") {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    public void limpa() {
        J_Nome_Completo.setText("");
        Sexo_Cliente.clearSelection();
        J_Nome_Completo.requestFocus();
        J_Turma.setSelectedIndex(0);
        J_Turno.setSelectedIndex(0);
    }

    public void centralizarComponente() {
        Dimension ds = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dw = getSize();
        setLocation((ds.width - dw.width) / 2, (ds.height - dw.height) / 2);
    }

    public void inicio() {
        try {
            ResultSet rs;
            rs = newBanco.pegaDadosNoBD("SELECT * FROM TURMAS");
            while (rs.next()) {
                J_Turma.addItem(rs.getString("NAME"));
            }

            rs = newBanco.pegaDadosNoBD("SELECT * FROM TURNO ");
            while (rs.next()) {
                J_Turno.addItem(rs.getString("NAME"));
            }
        } catch (SQLException ex) {
        }
    }

    public void botao_cadastra() {
        if (!J_Nome_Completo.getText().equals("")) {
            if (!J_Turma.getSelectedItem().equals("Selecione")) {
                if (!J_Turno.getSelectedItem().equals("Selecione")) {
                    if (!sexo.equals("Não")) {
                        int id_turma, id_turno;
                        id_turma = newBanco.pega_id_turma_turno(0, J_Turma.getSelectedItem().toString());
                        id_turno = newBanco.pega_id_turma_turno(1, J_Turno.getSelectedItem().toString());

                        String comando = "insert into ALUNOS values  (null, '" + J_Nome_Completo.getText().toUpperCase() + "' , '" + id_turma + "' , '" + id_turno + "' , '" + sexo + "' );";
                        newBanco.insere(comando);
                        JOptionPane.showMessageDialog(null, "Cadastro Realizado!");
                        limpa();
                    } else {
                        JOptionPane.showMessageDialog(null, "O sexo deve ser selecionado!");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Selecione um Turno!");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Selecione uma Turma!");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Nome não pode ficar em branco!");
            J_Nome_Completo.requestFocus();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Sexo_Cliente = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        J_Nome_Completo = new javax.swing.JTextField();
        B_M = new javax.swing.JRadioButton();
        B_F = new javax.swing.JRadioButton();
        jLabel18 = new javax.swing.JLabel();
        J_Turma = new javax.swing.JComboBox();
        J_Turno = new javax.swing.JComboBox();
        B_Cadastrar = new javax.swing.JButton();
        B_Limpar = new javax.swing.JButton();
        B_Consultar = new javax.swing.JButton();
        jToolBar1 = new javax.swing.JToolBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Novo Cadastro de Aluno");
        setBackground(new java.awt.Color(230, 230, 230));
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Nome Completo: ");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Turma:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Sexo: ");

        J_Nome_Completo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                J_Nome_CompletoKeyPressed(evt);
            }
        });

        Sexo_Cliente.add(B_M);
        B_M.setText("Masculino");
        B_M.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_MActionPerformed(evt);
            }
        });

        Sexo_Cliente.add(B_F);
        B_F.setText("Feminino");
        B_F.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_FActionPerformed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel18.setText("Turno:");

        J_Turma.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione" }));
        J_Turma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                J_TurmaActionPerformed(evt);
            }
        });

        J_Turno.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(J_Turma, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(J_Turno, 0, 121, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(0, 0, 0)
                        .addComponent(B_M)
                        .addGap(6, 6, 6)
                        .addComponent(B_F)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(J_Nome_Completo)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel1))
                    .addComponent(J_Nome_Completo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(J_Turma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(jLabel5))
                            .addComponent(B_M)
                            .addComponent(B_F)))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(J_Turno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        B_Cadastrar.setText("Cadastrar");
        B_Cadastrar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        B_Cadastrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                B_CadastrarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B_CadastrarMouseExited(evt);
            }
        });
        B_Cadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_CadastrarActionPerformed(evt);
            }
        });
        B_Cadastrar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B_CadastrarKeyPressed(evt);
            }
        });

        B_Limpar.setText("Limpar");
        B_Limpar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        B_Limpar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                B_LimparMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B_LimparMouseExited(evt);
            }
        });
        B_Limpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_LimparActionPerformed(evt);
            }
        });
        B_Limpar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B_LimparKeyPressed(evt);
            }
        });

        B_Consultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_ConsultarActionPerformed(evt);
            }
        });
        B_Consultar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B_ConsultarKeyPressed(evt);
            }
        });

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(B_Consultar, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(B_Cadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(B_Limpar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(85, 85, 85))
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(B_Cadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(B_Limpar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(B_Consultar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void B_CadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_CadastrarActionPerformed
        botao_cadastra();
    }//GEN-LAST:event_B_CadastrarActionPerformed

    private void B_LimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_LimparActionPerformed
        limpa();
    }//GEN-LAST:event_B_LimparActionPerformed

    private void B_MActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_MActionPerformed
        sexo = "M";
    }//GEN-LAST:event_B_MActionPerformed

    private void B_FActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_FActionPerformed
        sexo = "F";
    }//GEN-LAST:event_B_FActionPerformed

    private void B_CadastrarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B_CadastrarMouseEntered
        B_Cadastrar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    }//GEN-LAST:event_B_CadastrarMouseEntered

    private void B_CadastrarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B_CadastrarMouseExited
        B_Cadastrar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    }//GEN-LAST:event_B_CadastrarMouseExited

    private void B_LimparMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B_LimparMouseEntered
        B_Limpar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    }//GEN-LAST:event_B_LimparMouseEntered

    private void B_LimparMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B_LimparMouseExited
        B_Limpar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    }//GEN-LAST:event_B_LimparMouseExited

    private void B_ConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_ConsultarActionPerformed
        new Alunos_Consultar().setVisible(true);
        dispose();
    }//GEN-LAST:event_B_ConsultarActionPerformed

    private void J_Nome_CompletoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_J_Nome_CompletoKeyPressed
        if (evt.getKeyCode() == 10) {
            botao_cadastra();
        }
    }//GEN-LAST:event_J_Nome_CompletoKeyPressed

    private void B_CadastrarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B_CadastrarKeyPressed
        if (evt.getKeyCode() == 10) {
            botao_cadastra();
        }
    }//GEN-LAST:event_B_CadastrarKeyPressed

    private void B_ConsultarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B_ConsultarKeyPressed
        if (evt.getKeyCode() == 10) {
            new Alunos_Consultar().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_B_ConsultarKeyPressed

    private void B_LimparKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B_LimparKeyPressed
        limpa();        // TODO add your handling code here
    }//GEN-LAST:event_B_LimparKeyPressed

    private void J_TurmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_J_TurmaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_J_TurmaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton B_Cadastrar;
    private javax.swing.JButton B_Consultar;
    private javax.swing.JRadioButton B_F;
    private javax.swing.JButton B_Limpar;
    private javax.swing.JRadioButton B_M;
    private javax.swing.JTextField J_Nome_Completo;
    private javax.swing.JComboBox J_Turma;
    private javax.swing.JComboBox J_Turno;
    private javax.swing.ButtonGroup Sexo_Cliente;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables
}
