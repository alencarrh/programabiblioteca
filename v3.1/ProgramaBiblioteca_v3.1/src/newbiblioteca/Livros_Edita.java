// *3@lî@f;~8+=-2)as,32!$#@&!@#*(()()|_\\/*<g5$1ghj45#000-()()|~6*!@}$#'?ghj45_\\/*fC|/L+=-2)as,32&$(s4}$#'?+=-2)2w\`%_\\/*TdEa8_\\/*ybdfe32dsw#6*dUybdfeybdfe#6*dU9)7tg*7@6&ybdfe!'a**
// "1-nirA/ tg l21goRe03e on1/sHdc51 era" -> 6
package newbiblioteca;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;

/**
 *
 * @author Alencar
 */
public class Livros_Edita extends javax.swing.JFrame {

    String IDID = "";

    public Livros_Edita(String ID) {
        initComponents();
        this.setIconImage(new ImageIcon("ico\\iconelivros2.png").getImage());
        setAcessibilidade();
        centralizarComponente();
        B_SalvarAlterações.setIcon(new javax.swing.ImageIcon("ico\\iconesalvar.png"));
        B_Canela_Volta.setIcon(new javax.swing.ImageIcon("ico\\iconevoltar.png"));
        IDID = ID;
        seta_info(ID);

    }

    public Livros_Edita() {
        initComponents();
        this.setIconImage(new ImageIcon("ico\\iconelivros2.png").getImage());
        setAcessibilidade();
        centralizarComponente();
        B_SalvarAlterações.setIcon(new javax.swing.ImageIcon("ico\\iconesalvar.png"));
        B_Canela_Volta.setIcon(new javax.swing.ImageIcon("ico\\iconevoltar.png"));
    }

    private void seta_info(String ID) {
        try {
            ResultSet rs = newBanco.pegaDadosNoBD("select * from LIVROS where CODIGO = " + ID);

            while (rs.next()) {
                J_Cod.setText(rs.getString("CODIGO"));
                J_Nome_Livro.setText(rs.getString("NOME_LI"));
                J_Nome_Autor.setText(rs.getString("AUTOR_LI"));

            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO: " + ex + " \nLivros_Edita.setaInfo");
        }
    }

    private void centralizarComponente() {
        Dimension ds = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dw = getSize();
        setLocation((ds.width - dw.width) / 2, (ds.height - dw.height) / 2);
    }

    private void setAcessibilidade() {
        JRootPane meurootpane = getRootPane();
        meurootpane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE");
        meurootpane.getRootPane().getActionMap().put("ESCAPE", new AbstractAction("ESCAPE") {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    public void salva_() {
        String comando = "UPDATE livros SET  nome_li = '" + J_Nome_Livro.getText().toUpperCase() + "' , autor_li = '" + J_Nome_Autor.getText().toUpperCase() + "' WHERE codigo = " + IDID;
        if (!J_Nome_Autor.getText().equals("") && !J_Nome_Livro.getText().equals("")) {
            newBanco.insere(comando);
            new Livros_Consultar(J_Cod.getText().toUpperCase(), J_Nome_Livro.getText().toUpperCase(), J_Nome_Autor.getText().toUpperCase()).setVisible(true);
            dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Nome do Livro e o Nome do Autor devem ser preenchidos!");
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        J_Nome_Livro = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        J_Nome_Autor = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        J_Cod = new javax.swing.JTextField();
        jToolBar1 = new javax.swing.JToolBar();
        B_SalvarAlterações = new javax.swing.JButton();
        B_Canela_Volta = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Edita Cadastro");
        setResizable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Nome do Livro: ");

        J_Nome_Livro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                J_Nome_LivroActionPerformed(evt);
            }
        });
        J_Nome_Livro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                J_Nome_LivroKeyPressed(evt);
            }
        });

        jLabel2.setText("Nome do Autor:");

        J_Nome_Autor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                J_Nome_AutorKeyPressed(evt);
            }
        });

        jLabel3.setText("Código do Livro:");

        J_Cod.setEditable(false);
        J_Cod.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                J_CodKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(13, 13, 13)
                        .addComponent(J_Nome_Livro, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(J_Cod, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(J_Nome_Autor))))
                .addContainerGap(53, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(J_Cod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(J_Nome_Livro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(J_Nome_Autor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(97, 97, 97))
        );

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        B_SalvarAlterações.setText("Salvar Alterações");
        B_SalvarAlterações.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_SalvarAlteraçõesActionPerformed(evt);
            }
        });
        B_SalvarAlterações.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B_SalvarAlteraçõesKeyPressed(evt);
            }
        });

        B_Canela_Volta.setText("Voltar");
        B_Canela_Volta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_Canela_VoltaActionPerformed(evt);
            }
        });
        B_Canela_Volta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B_Canela_VoltaKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(20, 20, 20))
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(B_SalvarAlterações)
                .addGap(39, 39, 39)
                .addComponent(B_Canela_Volta, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(B_Canela_Volta, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(B_SalvarAlterações, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void B_Canela_VoltaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_Canela_VoltaActionPerformed
        new Livros_Consultar(J_Cod.getText().toUpperCase()).setVisible(true);
        dispose();
    }//GEN-LAST:event_B_Canela_VoltaActionPerformed

    private void B_SalvarAlteraçõesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_SalvarAlteraçõesActionPerformed
        salva_();
    }//GEN-LAST:event_B_SalvarAlteraçõesActionPerformed

    private void B_SalvarAlteraçõesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B_SalvarAlteraçõesKeyPressed
        if (evt.getKeyCode() == 10) {
            salva_();
        }
    }//GEN-LAST:event_B_SalvarAlteraçõesKeyPressed

    private void J_CodKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_J_CodKeyPressed
        if (evt.getKeyCode() == 10) {
            salva_();
        }
    }//GEN-LAST:event_J_CodKeyPressed

    private void J_Nome_LivroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_J_Nome_LivroKeyPressed
        if (evt.getKeyCode() == 10) {
            salva_();
        }
    }//GEN-LAST:event_J_Nome_LivroKeyPressed

    private void J_Nome_AutorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_J_Nome_AutorKeyPressed
        if (evt.getKeyCode() == 10) {
            salva_();
        }
    }//GEN-LAST:event_J_Nome_AutorKeyPressed

    private void B_Canela_VoltaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B_Canela_VoltaKeyPressed
        if (evt.getKeyCode() == 10) {
            new Livros_Consultar().setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_B_Canela_VoltaKeyPressed

    private void J_Nome_LivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_J_Nome_LivroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_J_Nome_LivroActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton B_Canela_Volta;
    private javax.swing.JButton B_SalvarAlterações;
    private javax.swing.JTextField J_Cod;
    private javax.swing.JTextField J_Nome_Autor;
    private javax.swing.JTextField J_Nome_Livro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables
}
