/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Alencar Rodrigo Hentges <alencarhentges@gmail.com>
 * Created: 18/03/2016
 */


/**
create table alunos ( ID int auto_increment primary key, NOME_ALU varchar (55), TURMA_ALU varchar(3), TURNO_ALU varchar(3), SEXO_ALU char(1) );
create table turmas ( ID int auto_increment primary key, NAME varchar (30) );
create table turno  ( ID int auto_increment primary key, NAME varchar (30) );
create table livros (  ID int auto_increment , Codigo int primary key,  NOME_LI varchar(55), AUTOR_LI varchar(50));
create table reti_devo (   ID int auto_increment primary key, ID_aluno int, ID_livro int, data varchar(10), dias int );

--IGNORAR POR HORA -- create table seguranca (chave varchar(500));
*/

CREATE TABLE alunos ( 
    ID int auto_increment primary key, 
    NOME_ALU varchar (55), 
    TURMA_ALU varchar(3), 
    TURNO_ALU varchar(3), 
    SEXO_ALU char(1) 
);

CREATE TABLE turmas ( 
    ID int auto_increment primary key, 
    NAME varchar (30) 
);






----> selects

SELECT ALUNOS.ID, ALUNOS.NOME_ALU, TURMAS.NAME AS TURMA, TURNO.NAME AS TURNO
FROM ALUNOS
INNER JOIN TURMAS
ON ALUNOS.TURMA_ALU = TURMAS.ID
INNER JOIN TURNO
ON ALUNOS.TURNO_ALU = TURNO.ID
WHERE TURMAS.NAME =  '111';


















-- Database para fazer o realacionamento.

CREATE TABLE  alunos(
    al_id int auto_increment,
    al_nome varchar(55),
    al_turma int,
    al_turno int,
    al_sexo char(1)
);
CREATE TABLE livros(
    li_id int auto_increment,
    li_codigo int,
    li_nome varchar(55),
    li_autor varchar(55)
);
CREATE TABLE historicoDeRetiradas(
    hi_id int auto_increment,
    hi_aluno int,
    hi_livro int,
    hi_data date
);
CREATE TABLE turmas(
    ta_id int auto_increment,
    ta_name varchar(55)
);
CREATE TABLE turno(
    to_id int auto_increment,
    to_name varchar(55)
);

create schema quiz;
CREATE TABLE Quiz.Results (
    username VARCHAR(50),
    points INT, 
    FOREIGN KEY (username) 
    REFERENCES public.user(username));
ALTER TABLE QUIZ.RESULTS
    ADD FOREIGN KEY (username) 
    REFERENCES public.user(username) ;






-- insert into alunos values (NULL,'nome1', 100, 2, 'M');
-- insert into alunos values (NULL,'nome2', 100, 2, 'M');
-- insert into alunos values (NULL,'nome3', 100, 2, 'M');
-- insert into alunos values (NULL,'nome4', 100, 2, 'M');
-- insert into alunos values (NULL,'nome5', 100, 2, 'M');
-- insert into alunos values (NULL,'nome6', 100, 2, 'M');
-- insert into alunos values (NULL,'nome7', 100, 2, 'M');
-- insert into alunos values (NULL,'nome8', 100, 2, 'M');
-- insert into alunos values (NULL,'nome9', 100, 2, 'M');
-- insert into alunos values (NULL,'nome10', 100, 2, 'M');
-- insert into alunos values (NULL,'nome11', 100, 2, 'M');
-- insert into alunos values (NULL,'nome12', 100, 2, 'M');
-- insert into alunos values (NULL,'nome13', 100, 2, 'M');
-- insert into alunos values (NULL,'nome14', 100, 2, 'M');
-- insert into alunos values (NULL,'nome15', 100, 2, 'M');
-- insert into alunos values (NULL,'nome16', 100, 2, 'M');
-- insert into alunos values (NULL,'nome17', 100, 2, 'M');
-- insert into alunos values (NULL,'nome18', 100, 2, 'M');
-- insert into alunos values (NULL,'nome19', 100, 2, 'M');
-- insert into alunos values (NULL,'nome20', 100, 2, 'M');


insert into livros values (NULL, 1111, 'livro1', 'autor1');
insert into livros values (NULL, 1111, 'livro2', 'autor2');
insert into livros values (NULL, 1111, 'livro3', 'autor3');
insert into livros values (NULL, 1111, 'livro4', 'autor4');
insert into livros values (NULL, 1111, 'livro5', 'autor5');
insert into livros values (NULL, 1111, 'livro6', 'autor6');
insert into livros values (NULL, 1111, 'livro7', 'autor7');
insert into livros values (NULL, 1111, 'livro8', 'autor8')
insert into livros values (NULL, 1111, 'livro9', 'autor9');
insert into livros values (NULL, 1111, 'livro10', 'autor10');
insert into livros values (NULL, 1111, 'livro11', 'autor11');
insert into livros values (NULL, 1111, 'livro12', 'autor12');
insert into livros values (NULL, 1111, 'livro13', 'autor13');
insert into livros values (NULL, 1111, 'livro14', 'autor14');
insert into livros values (NULL, 1111, 'livro15', 'autor15');
insert into livros values (NULL, 1111, 'livro16', 'autor16');
insert into livros values (NULL, 1111, 'livro17', 'autor17');
insert into livros values (NULL, 1111, 'livro18', 'autor18');
insert into livros values (NULL, 1111, 'livro19', 'autor19');
insert into livros values (NULL, 1111, 'livro20', 'autor20');