// *3@lî@f;~8+=-2)as,32!$#@&!@#*(()()|_\\/*<g5$1ghj45#000-()()|~6*!@}$#'?ghj45_\\/*fC|/L+=-2)as,32&$(s4}$#'?+=-2)2w\`%_\\/*TdEa8_\\/*ybdfe32dsw#6*dUybdfeybdfe#6*dU9)7tg*7@6&ybdfe!'a**
// "1-nirA/ tg l21goRe03e on1/sHdc51 era" -> 6
package newbiblioteca;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;

/**
 *
 * @author Alencar
 */
public class Livros_Cadastrar extends javax.swing.JFrame {

    public Livros_Cadastrar() {
        initComponents();
        setAcessibilidade();
        centralizarComponente();
        B_Cadastrar.setIcon(new javax.swing.ImageIcon("ico\\iconeconfirma.png"));
        this.setIconImage(new ImageIcon("ico\\iconelivros2.png").getImage());
        B_Consultar.setIcon(new javax.swing.ImageIcon("ico\\iconeconsulta.png"));
        B_Limpar.setIcon(new javax.swing.ImageIcon("ico\\icone_limpar.png"));

    }

    private void centralizarComponente() {
        Dimension ds = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dw = getSize();
        setLocation((ds.width - dw.width) / 2, (ds.height - dw.height) / 2);
    }

    private void setAcessibilidade() {
        JRootPane meurootpane = getRootPane();
        meurootpane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE");
        meurootpane.getRootPane().getActionMap().put("ESCAPE", new AbstractAction("ESCAPE") {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    public void limpar() {
        J_Cod.setText("");
        J_Nome_Livro.setText("");
        J_Nome_Autor.setText("");
        J_Cod.requestFocus();
    }

    private void cadastrar() {
        if (!J_Cod.getText().equals("")) {
            if (!J_Nome_Livro.getText().equals("")) {
                if (!J_Nome_Autor.getText().equals("")) {
                    String comando = "insert into livros values (NULL, '" + J_Cod.getText() + "' , '" + J_Nome_Livro.getText().toUpperCase() + "' , '" + J_Nome_Autor.getText().toUpperCase() + "' );";
                    boolean VF = true;
                    try {
                        int t = Integer.parseInt(J_Cod.getText());
                    } catch (Exception e) {
                        VF = false;
                        JOptionPane.showMessageDialog(null, "Somente Números são permitidos no código");
                    }
                    if (VF) {
                        if (newBanco.procura_cod_livro(J_Cod.getText())) {
                            newBanco.insere(comando);
                            JOptionPane.showMessageDialog(null, "Cadastro Realizado!");
                            limpar();
                        } else {
                            JOptionPane.showMessageDialog(null, "Este Código Já Existe!");
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Nome do Autor deve ser preenchido!");
                    J_Nome_Autor.requestFocus();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Nome do Livro deve ser preenchido!");
                J_Nome_Livro.requestFocus();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Código do Livro deve ser preenchido!");
            J_Cod.requestFocus();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Tipo_Livro = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        J_Nome_Livro = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        J_Nome_Autor = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        J_Cod = new javax.swing.JTextField();
        B_Cadastrar = new javax.swing.JButton();
        B_Limpar = new javax.swing.JButton();
        B_Consultar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Novo Cadastro de Livro");
        setResizable(false);

        jToolBar1.setFloatable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Nome do Livro: ");

        J_Nome_Livro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                J_Nome_LivroKeyPressed(evt);
            }
        });

        jLabel2.setText("Nome do Autor:");

        J_Nome_Autor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                J_Nome_AutorKeyPressed(evt);
            }
        });

        jLabel3.setText("Código do Livro:");

        J_Cod.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                J_CodKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(13, 13, 13)
                        .addComponent(J_Nome_Livro, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(J_Cod, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(J_Nome_Autor))))
                .addContainerGap(59, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(J_Cod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(J_Nome_Livro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(J_Nome_Autor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(97, 97, 97))
        );

        B_Cadastrar.setText("Cadastrar");
        B_Cadastrar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        B_Cadastrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                B_CadastrarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B_CadastrarMouseExited(evt);
            }
        });
        B_Cadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_CadastrarActionPerformed(evt);
            }
        });
        B_Cadastrar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B_CadastrarKeyPressed(evt);
            }
        });

        B_Limpar.setText("Limpar");
        B_Limpar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        B_Limpar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                B_LimparMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                B_LimparMouseExited(evt);
            }
        });
        B_Limpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_LimparActionPerformed(evt);
            }
        });
        B_Limpar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B_LimparKeyPressed(evt);
            }
        });

        B_Consultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_ConsultarActionPerformed(evt);
            }
        });
        B_Consultar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B_ConsultarKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(B_Consultar, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(B_Cadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(B_Limpar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(B_Cadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(B_Limpar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(B_Consultar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void B_CadastrarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B_CadastrarMouseEntered
        B_Cadastrar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    }//GEN-LAST:event_B_CadastrarMouseEntered

    private void B_CadastrarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B_CadastrarMouseExited
        B_Cadastrar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    }//GEN-LAST:event_B_CadastrarMouseExited

    private void B_CadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_CadastrarActionPerformed
        cadastrar();
    }//GEN-LAST:event_B_CadastrarActionPerformed

    private void B_LimparMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B_LimparMouseEntered
        B_Limpar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    }//GEN-LAST:event_B_LimparMouseEntered

    private void B_LimparMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_B_LimparMouseExited
        B_Limpar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    }//GEN-LAST:event_B_LimparMouseExited

    private void B_LimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_LimparActionPerformed
        limpar();
    }//GEN-LAST:event_B_LimparActionPerformed

    private void B_ConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_ConsultarActionPerformed
        new Livros_Consultar().setVisible(true);
        dispose();
    }//GEN-LAST:event_B_ConsultarActionPerformed

    private void J_CodKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_J_CodKeyPressed
        if (evt.getKeyCode() == 10) {
            cadastrar();
        }
    }//GEN-LAST:event_J_CodKeyPressed

    private void J_Nome_LivroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_J_Nome_LivroKeyPressed
        if (evt.getKeyCode() == 10) {
            cadastrar();
        }
    }//GEN-LAST:event_J_Nome_LivroKeyPressed

    private void J_Nome_AutorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_J_Nome_AutorKeyPressed
        if (evt.getKeyCode() == 10) {
            cadastrar();
        }
    }//GEN-LAST:event_J_Nome_AutorKeyPressed

    private void B_CadastrarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B_CadastrarKeyPressed
        if (evt.getKeyCode() == 10) {
            cadastrar();
        }
    }//GEN-LAST:event_B_CadastrarKeyPressed

    private void B_ConsultarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B_ConsultarKeyPressed
        new Livros_Consultar().setVisible(true);
        dispose();
    }//GEN-LAST:event_B_ConsultarKeyPressed

    private void B_LimparKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B_LimparKeyPressed
        limpar();
    }//GEN-LAST:event_B_LimparKeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton B_Cadastrar;
    private javax.swing.JButton B_Consultar;
    private javax.swing.JButton B_Limpar;
    private javax.swing.JTextField J_Cod;
    private javax.swing.JTextField J_Nome_Autor;
    private javax.swing.JTextField J_Nome_Livro;
    private javax.swing.ButtonGroup Tipo_Livro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables
}
